/*
   inhomog - biscale kinematical backreaction analytical evolution

   Copyright (C) 2012-2019 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <sys/types.h>
#include <argp.h>
#include "config.h"

#include "lib/inhomog.h"
#include "lib/parse_noempty_strtox.h"
#include "lib/biscale_partition.h"


#define EXIT_FAILURE 1

#if ENABLE_NLS
# include <libintl.h>
# define _(Text) gettext (Text)
#else
# define textdomain(Domain)
# define _(Text) Text
#endif
#define N_(Text) Text

#define DEBUG 1
#undef DEBUG

/*
  char *xmalloc ();
  char *xrealloc ();
  char *xstrdup ();
*/

static error_t parse_opt (int key, char *arg, struct argp_state *state);
static void show_version (FILE *stream, struct argp_state *state);

/* argp option keys */
enum {DUMMY_KEY=129
};

/* Option flags and variables.  These are initialized in parse_opt.  */

int want_verbose;               /* --verbose */
enum collapse_types collapse_type;
long collapse_type_long;
double I_inv_double;
double II_inv_double;
double III_inv_double;
double I_inv[N_TWO];
double II_inv[N_TWO];
double III_inv[N_TWO];

int want_expanding;
int want_collapsing;
int want_global;

char *endptr = NULL;  /*  error handling by   strtod, strtol  -  info strtod  */

static struct argp_option options[] =
{
  { "verbose",     'v',           NULL,            0,
    N_("Print more information"), 0 },
  { "biscale",    'b',           N_("COLLAPSE_TYPE"),            0,
    N_("Biscale collapse type ([0] planar, 1 spher, 2 <III>_i=0, 3 <II>_i=0)"), 0 },
  { "I",     '1',           N_("I_D_INIT"),            0,
    N_("initial mean I in domain D"), 0 },
  { "II",     '2',           N_("II_D_INIT"),            0,
    N_("initial mean II in domain D"), 0 },
  { "III",     '3',           N_("III_D_INIT"),            0,
    N_("initial mean III in domain D"), 0 },
  { "expanding",     'e',           NULL,            0,
    N_("output [E]xpanding domain VQZA and Newtonian scale factors"), 0 },
  { "collapsing",     'c',           NULL,            0,
    N_("output Collapsing domain VQZA and Newtonian scale factors"), 0 },
  { "global",     'g',           NULL,            0,
    N_("output Global VQZA and Newtonian scale factors"), 0 },
  { NULL, 0, NULL, 0, NULL, 0 }
};

/* The argp functions examine these global variables.  */
const char *argp_program_bug_address = "boud cosmo.torun.pl";
void (*argp_program_version_hook) (FILE *, struct argp_state *) = show_version;

static struct argp argp =
{
  /* options, parse_opt, N_("[FILE...]"), */
  options, parse_opt, NULL,
  N_("biscale kinematical backreaction analytical evolution\n\nThe inhomog library provides Raychaudhuri integration of cosmological domain-wise average scale factor evolution using an analytical formula for kinematical backreaction Q_D evolution. The inhomog main program illustrates biscale examples. The library routine lib/Omega_D_precalc.c is callable by RAMSES using RAMSES-SCALAV (see Roukema 2018 A&A 610, A51, arXiv:1706.06179).\n\nSource:\ngit clone https://broukema@bitbucket.org/broukema/inhomog.git"),
  NULL, NULL, NULL
};

int
main (int argc, char **argv)
{

  double t_background[N_TIMES];
  double a_FLRW[N_TIMES];
  double a_global_QZA[N_TIMES];
  double a_D[N_TWO][N_TIMES]; /* a_D local QZA */
  double a_D_Newt[N_TWO][N_TIMES]; /* a_D local Newt */

  int32_t i_t;
  int i_warn1=0;

  textdomain(PACKAGE);
  argp_parse(&argp, argc, argv, 0, NULL, NULL);

  if(!want_expanding && !want_collapsing && !want_global)
    want_expanding = 1; /* default */

  biscale_partition(collapse_type,
                    want_verbose,
                    I_inv,
                    II_inv,
                    III_inv,
                    t_background,
                    a_FLRW,
                    a_global_QZA,
                    a_D,
                    a_D_Newt
                    );

  for (i_t=0; i_t<N_TIMES; i_t++){
    if(want_expanding){
      printf("%g %15.10g %15.10g %15.10g\n",
             t_background[i_t],
             a_FLRW[i_t],
             a_D[0][i_t],
             a_D_Newt[0][i_t]);
    }else if(want_collapsing){
      printf("%g %15.10g %15.10g %15.10g\n",
             t_background[i_t],
             a_FLRW[i_t],
             a_D[1][i_t],
             a_D_Newt[1][i_t]);
    }else if(want_global){
      printf("%g %15.10g %15.10g\n",
             t_background[i_t],
             a_FLRW[i_t],
             a_global_QZA[i_t]);
    }else if(i_warn1 < 1){
      printf("Please select an output option.\n");
      i_warn1 ++;
    };
  }; /* for (i_t=0; i_t<N_TIMES; i_t++) */

  exit(0);
}

/* Parse a single option.  */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct argp_state *dummy; /* avoid strict ISO 90 C complaints */
  dummy = state;
  (void)dummy; /* avoid set-but-not-used compile complaint */
  switch (key)
    {
    case ARGP_KEY_INIT:
      /* Set up default values.  */
      want_verbose = 0;
      collapse_type = planar_collapse;
      want_expanding = 0;
      want_collapsing = 0;
      want_global = 0;
      /* Expanding domain initial invariants; some may be overwritten. */
      I_inv[0] = 0.01;
      II_inv[0] = 1e-4;
      III_inv[0] = 1e-6;
      break;

    case 'v':                   /* --verbose */
      want_verbose = 1;
      break;

    case 'b':                   /* --biscale */
      if(arg){  /* are there any parameters given to this option? */
        errno=parse_noempty_strtox(arg, "inhomog:", "--biscale COLLAPSE_TYPE", 'l',0,
                                   &collapse_type_long, &endptr);
        if(0==errno)
          collapse_type = (enum collapse_types)collapse_type_long;

      }; /* if(arg) */
      break;

    case 'e':
      if(want_verbose && (want_collapsing || want_global)){
        printf("Warning: only --expanding option accepted.\n");
      };
      want_expanding = 1;
      break;
    case 'c':
      if(want_verbose && (want_expanding || want_global)){
        printf("Warning: only -collapsing option accepted.\n");
      };
      want_collapsing = 1;
      break;
    case 'g':
      if(want_verbose && (want_collapsing || want_expanding)){
        printf("Warning: only --global option accepted.\n");
      };
      want_global = 1;
      break;

    case '1':                   /* --I */
      if(arg){  /* are there any parameters given to this option? */
        errno=parse_noempty_strtox(arg, "inhomog:", "--I I_INV_INIT", 'd',0,
                                   &I_inv_double, &endptr);
        if(0==errno)
          I_inv[0] = I_inv_double;

      }; /* if(arg) */
      break;

    case '2':                   /* --II */
      if(arg){  /* are there any parameters given to this option? */
        errno=parse_noempty_strtox(arg, "inhomog:", "--II II_INV_INIT", 'd',0,
                                   &II_inv_double, &endptr);
        if(0==errno)
          II_inv[0] = II_inv_double;

      }; /* if(arg) */
      break;

    case '3':                   /* --III */
      if(arg){  /* are there any parameters given to this option? */
        errno=parse_noempty_strtox(arg, "inhomog:", "--III III_INV_INIT", 'd',0,
                                   &III_inv_double, &endptr);
        if(0==errno)
          III_inv[0] = III_inv_double;

      }; /* if(arg) */
      break;

    case ARGP_KEY_ARG:          /* [FILE]... */
      /* TODO: Do something with ARG, or remove this case and make
         main give argp_parse a non-NULL fifth argument.  */
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

/* Show the version number and copyright information.  */
static void
show_version (FILE *stream, struct argp_state *state)
{
  (void) state;
  /* Print in small parts whose localizations can hopefully be copied
     from other programs.  */
  fputs(PACKAGE" "VERSION"\n", stream);
  fprintf(stream, _("Written by %s.\n\n"), "Boud Roukema, Jan Ostrowski");
  fprintf(stream, _("Copyright (C) %s %s\n"), "2012-2019", "Boud Roukema, Jan Ostrowski");
  fputs(_("\
This program is free software; you may redistribute it under the terms of\n\
the GNU General Public License version 2 or later.  This program has absolutely no warranty.\n"),
        stream);
}
