/*
   FLRW background model functions - some may be wrappers

   Copyright (C) 2013 Jan Ostrowski, Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file FLRW_background.c */

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <gsl/gsl_rng.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif

#include "lib/inhomog.h"

/* TODO: error handling if wrong model is used */

/*! \brief Calculates scale factor for the EdS model.
 *
 * The scale factor is calculated from the expression:
 * \f$ a_{EdS} = a_0 e^{\frac{2}{3} log(\frac{t}{t_0})}\f$, where
 * \f$ a_0 \f$ is the present scale factor value (not yet normalized to
 * 1) and \f$ t \f$ is the time value from \a t_background.
 *
 * If \a recalculate_t_0 is set to 1 in background_cosm_params_s
 * structure, recalculates \f$ t_0 \f$ from the Hubble constant and sets
 * \a recalculate_t_0 to 0.
 *
 * If \b DEBUG is enabled, prints out an error if the \a t_background
 * values are too big or too small.
 *
 * \param [in] background_cosm_params pointer to the
 * background_cosm_params_s containing relevant cosmological parameters
 * \param [in] t_background time
 * \param [in] want_verbose control parameter; defined and explained in
 * biscale_partition.c
 */
double a_EdS(/* INPUTS: */
             struct background_cosm_params_s *background_cosm_params,
             double t_background,
             int want_verbose
             /* OUTPUTS: */
             ){
  double a_scale_factor;
  const double two_thirds = 2.0/3.0;

  if(!(background_cosm_params->EdS) && want_verbose){
    printf("a_EdS Warning: called for non-EdS bg model.\n");
    exit(1);
  };

  if(background_cosm_params->recalculate_t_0){
    background_cosm_params->t_0 = two_thirds /
      (background_cosm_params->H_0 * COSM_H_0_INV_GYR);
    background_cosm_params->recalculate_t_0 = 0;
  };


#ifdef DEBUG
  if(!(t_background > 1e-8 && t_background < 100.0) ||
     !(background_cosm_params->t_0 > 1e-8 && background_cosm_params->t_0 < 100.0)){
    printf("a_EdS ERROR: t_background, background_cosm_params->t_0 = %g %g\n",
           t_background, background_cosm_params->t_0);
  };
#endif
  a_scale_factor = exp(two_thirds*
                     log(t_background/background_cosm_params->t_0))
    * background_cosm_params->inhomog_a_scale_factor_now;
  return a_scale_factor;
}
/*! \brief Calculates t_background for the EdS model.
 *
 *
 *
 * \param [in] background_cosm_params pointer to the
 * background_cosm_params_s containing relevant cosmological parameters
 * \param [in] t_background time
 * \param [in] want_verbose control parameter; defined and explained in
 * biscale_partition.c
 */
double t_EdS(/* INPUTS: */
             struct background_cosm_params_s *background_cosm_params,
             double a_scale_factor,
             int want_verbose
             /* OUTPUTS: */
             ){
  double t_background;
  const double two_thirds = 2.0/3.0;

  if(!(background_cosm_params->EdS) && want_verbose){
    printf("t_EdS Warning: called for non-EdS bg model.\n");
  };

  if(background_cosm_params->recalculate_t_0){
    background_cosm_params->t_0 = two_thirds /
      (background_cosm_params->H_0 * COSM_H_0_INV_GYR);
    background_cosm_params->recalculate_t_0 = 0;
  };

  t_background =
    exp(1.5*log( a_scale_factor /background_cosm_params->inhomog_a_scale_factor_now)) *
    background_cosm_params->t_0;
  return t_background;
}

/*! \brief Calculates first derivative of the scale factor for the EdS
 * model.
 *
 * Derivative is calculated from \ref a_EdS scale factor function.
 *
 * If the model used is not an Einstein de Sitter model and if
 * \a want_verbose parameter is set to 1, prints out an error.
 *
 * If \a recalculate_t_0 is set to 1 in background_cosm_params_s
 * structure, recalculates \f$ t_0 \f$ from the Hubble constant and sets
 * \a recalculate_t_0 to 0.
 *
 * \param [in] background_cosm_params pointer to the
 * background_cosm_params_s containing relevant cosmological parameters
 * \param [in] t_background time
 * \param [in] want_verbose control parameter; defined and explained in
 * biscale_partition.c
 */
double a_dot_EdS(/* INPUTS: */
                 struct background_cosm_params_s * background_cosm_params,
                 double t_background,
                 int want_verbose
                 /* OUTPUTS: */
                 ){
  double a_scale_factor_dot;
  const double minus_one_third = -1.0/3.0;
  const double two_thirds = 2.0/3.0;

  if(!(background_cosm_params->EdS) && want_verbose){
    printf("a_EdS Warning: called for non-EdS bg model.\n");
  };

  if(background_cosm_params->recalculate_t_0){
    background_cosm_params->t_0 = two_thirds /
      (background_cosm_params->H_0 * COSM_H_0_INV_GYR);
    background_cosm_params->recalculate_t_0 = 0;
  };

  a_scale_factor_dot = two_thirds *
    exp(minus_one_third* log(t_background/background_cosm_params->t_0) ) /
    background_cosm_params->t_0 *
    background_cosm_params->inhomog_a_scale_factor_now;
  return a_scale_factor_dot;
}

/*! \brief Calculates second derivative of the scale factor for the EdS
 * model.
 *
 * Derivative is calculated from \ref a_EdS scale factor function.
 *
 * If the model used is not an Einstein de Sitter model and if
 * \a want_verbose parameter is set to 1, prints out an error.
 *
 * If \a recalculate_t_0 is set to 1 in background_cosm_params_s
 * structure, recalculates \f$ t_0 \f$ from the Hubble constant and sets
 * \a recalculate_t_0 to 0.
 *
 * \param [in] background_cosm_params pointer to the
 * background_cosm_params_s containing relevant cosmological parameters
 * \param [in] t_background time
 * \param [in] want_verbose control parameter; defined and explained in
 * biscale_partition.c
 */
double a_ddot_EdS(/* INPUTS: */
                 struct background_cosm_params_s * background_cosm_params,
                 double t_background,
                 int want_verbose
                 /* OUTPUTS: */
                 ){
  double a_scale_factor_ddot;
  const double minus_four_thirds = -4.0/3.0;
  const double two_thirds = 2.0/3.0;
  const double minus_two_ninths = -2.0/9.0;
  if(!(background_cosm_params->EdS) && want_verbose){
    printf("a_EdS Warning: called for non-EdS bg model.\n");
  };

  if(background_cosm_params->recalculate_t_0){
    background_cosm_params->t_0 = two_thirds /
      (background_cosm_params->H_0 * COSM_H_0_INV_GYR);
    background_cosm_params->recalculate_t_0 = 0;
  };

  a_scale_factor_ddot = minus_two_ninths *
    exp(minus_four_thirds* log(t_background/background_cosm_params->t_0))  /
    (background_cosm_params->t_0 * background_cosm_params->t_0) *
    background_cosm_params->inhomog_a_scale_factor_now;
  return a_scale_factor_ddot;
}



/* See Sahni & Starobinsky 2000 IJMPD
   http://cdsads.u-strasbg.fr/abs/2000IJMPD...9..373S
   http://ned.ipac.caltech.edu/level5/March02/Sahni/Sahni3_2.html
   for the exact expressions for a(t) for flat, non-zero Lambda FLRW.
*/

/*! \brief Calculates the t_background for flat FLRW background.
 *
 * If \a recalculate_t_0 is set to 1 in background_cosm_params_s
 * structure, recalculates \f$ t_0 \f$ from the Hubble constant and sets
 * \a recalculate_t_0 to 0.
 *
 * If the model used is not a FLRW model and if
 * \a want_verbose parameter is set to 1, prints out an error.
 *
 * \a t_background is calculated from:
 * \f$ a(t) \propto \bigg(sinh \frac{3}{2} \sqrt{\frac{\Lambda}{3}} ct
 * \bigg)^{\frac{2}{3}} \f$
 * (Sahni & Starobinsky, 2000 \latexonly ,
 * \href{http://ned.ipac.caltech.edu/level5/March02/Sahni/Sahni3_2.html}
 * {IJMPD} \endlatexonly ).
 *
 * \param [in] background_cosm_params pointer to the
 * background_cosm_params_s containing relevant cosmological parameters
 * \param [in] a_scale_factor flat FLRW scale factor
 * \param [in] want_verbose control parameter; defined and explained in
 * biscale_partition.c
 */
double t_flatFLRW(/* INPUTS: */
                  struct background_cosm_params_s * background_cosm_params,
                  double a_scale_factor,
                  int want_verbose
                  /* OUTPUTS: */
                  ){
  double t_background;
  const double two_thirds = 2.0/3.0;

  if((1==background_cosm_params->EdS || !(background_cosm_params->flatFLRW) || background_cosm_params->Omm_0 > 1.0) && want_verbose){
    printf("t_flatFLRW Warning: called for invalid bg model.\n");
  };

  if(background_cosm_params->recalculate_t_0){
    background_cosm_params->H_0_sqrt_OmLam_0 =
      background_cosm_params->H_0 * COSM_H_0_INV_GYR *
      sqrt(background_cosm_params->OmLam_0);

    background_cosm_params->t_0 = two_thirds *
      atanh(sqrt(background_cosm_params->OmLam_0))/
      background_cosm_params->H_0_sqrt_OmLam_0;

    background_cosm_params->H_0_sqrt_OmLam_0_threehalves_t_0 =
      background_cosm_params->H_0_sqrt_OmLam_0 * 1.5 *
      background_cosm_params->t_0;

    background_cosm_params->recalculate_t_0 = 0;
  };

  t_background = two_thirds *
    asinh(
          exp(1.5*log( a_scale_factor /background_cosm_params->inhomog_a_scale_factor_now)) *
          sinh(background_cosm_params->H_0_sqrt_OmLam_0_threehalves_t_0)) /
    background_cosm_params->H_0_sqrt_OmLam_0;
  return t_background;
}

/*! \brief Calculates the scale factor of a flat FLRW model.
 *
 * If \a recalculate_t_0 is set to 1 in background_cosm_params_s
 * structure, recalculates \f$ t_0 \f$ from the Hubble constant and sets
 * \a recalculate_t_0 to 0.
 *
 * If the model used is not a FLRW model and if
 * \a want_verbose parameter is set to 1, prints out an error.
 *
 * The scale factor is calculated from:
 * \f$ a(t) \propto \bigg(sinh \frac{3}{2} \sqrt{\frac{\Lambda}{3}} ct
 * \bigg)^{\frac{2}{3}} \f$
 * (Sahni & Starobinsky, 2000 \latexonly ,
 * \href{http://ned.ipac.caltech.edu/level5/March02/Sahni/Sahni3_2.html}
 * {IJMPD} \endlatexonly ).
 *
 * \param [in] background_cosm_params pointer to the
 * background_cosm_params_s containing relevant cosmological parameters
 * \param [in] t_background time
 * \param [in] want_verbose control parameter; defined and explained in
 * biscale_partition.c
 */
double a_flatFLRW(/* INPUTS: */
                  struct background_cosm_params_s * background_cosm_params,
                  double t_background,
                  int want_verbose
                  /* OUTPUTS: */
                  ){
  double a_scale_factor;
  const double two_thirds = 2.0/3.0;

  if((1==background_cosm_params->EdS || !(background_cosm_params->flatFLRW) || background_cosm_params->Omm_0 > 1.0) && want_verbose){
    printf("t_flatFLRW Warning: called for invalid bg model.\n");
  };

  /* TODO: modularise this common section */
  if(background_cosm_params->recalculate_t_0){
    background_cosm_params->H_0_sqrt_OmLam_0 =
      background_cosm_params->H_0 * COSM_H_0_INV_GYR *
      sqrt(background_cosm_params->OmLam_0);

    background_cosm_params->t_0 = two_thirds *
      atanh(sqrt(background_cosm_params->OmLam_0))/
      background_cosm_params->H_0_sqrt_OmLam_0;

    background_cosm_params->H_0_sqrt_OmLam_0_threehalves_t_0 =
      background_cosm_params->H_0_sqrt_OmLam_0 * 1.5 *
      background_cosm_params->t_0;

    background_cosm_params->recalculate_t_0 = 0;
  };

  a_scale_factor =
    exp(two_thirds * log
        (sinh( 1.5 * background_cosm_params->H_0_sqrt_OmLam_0 *
               t_background ) /
         sinh( background_cosm_params->H_0_sqrt_OmLam_0_threehalves_t_0) ))
    * background_cosm_params->inhomog_a_scale_factor_now;
  return a_scale_factor;
}


/*! \brief Calculates the first derivative of the scale factor of a flat FLRW
 * model.
 *
 * If \a recalculate_t_0 is set to 1 in background_cosm_params_s
 * structure, recalculates \f$ t_0 \f$ from the Hubble constant and sets
 * \a recalculate_t_0 to 0.
 *
 * If the model used is not a FLRW model and if
 * \a want_verbose parameter is set to 1, prints out an error.
 *
 * The scale factor is calculated from:
 * \f$ a(t) \propto \bigg(sinh \frac{3}{2} \sqrt{\frac{\Lambda}{3}} ct
 * \bigg)^{\frac{2}{3}} \f$
 * (Sahni & Starobinsky, 2000 \latexonly ,
 * \href{http://ned.ipac.caltech.edu/level5/March02/Sahni/Sahni3_2.html}
 * {IJMPD} \endlatexonly ).
 *
 * \param [in] background_cosm_params pointer to the
 * background_cosm_params_s containing relevant cosmological parameters
 * \param [in] t_background time
 * \param [in] want_verbose control parameter; defined and explained in
 * biscale_partition.c
 */
double a_dot_flatFLRW(/* INPUTS: */
                  struct background_cosm_params_s * background_cosm_params,
                  double t_background,
                  int want_verbose
                  /* OUTPUTS: */
                  ){
  double a_scale_factor_dot;
  const double two_thirds = 2.0/3.0;
  const double minus_one_third = -1.0/3.0;

  if((1==background_cosm_params->EdS || !(background_cosm_params->flatFLRW) || background_cosm_params->Omm_0 > 1.0) && want_verbose){
    printf("t_flatFLRW Warning: called for invalid bg model.\n");
  };

  /* TODO: modularise this common section */
  if(background_cosm_params->recalculate_t_0){
    background_cosm_params->H_0_sqrt_OmLam_0 =
      background_cosm_params->H_0 * COSM_H_0_INV_GYR *
      sqrt(background_cosm_params->OmLam_0);

    background_cosm_params->t_0 = two_thirds *
      atanh(sqrt(background_cosm_params->OmLam_0))/
      background_cosm_params->H_0_sqrt_OmLam_0;

    background_cosm_params->H_0_sqrt_OmLam_0_threehalves_t_0 =
      background_cosm_params->H_0_sqrt_OmLam_0 * 1.5 *
      background_cosm_params->t_0;

    background_cosm_params->recalculate_t_0 = 0;
  };

  /*
    maxima: where the constant C = H_0 \sqrt(\Omega_{\Lambda 0})
    a : (sinh(3/2* C *t)/sinh(3/2* C *t0))^(2/3);
    diff(a,t,1);
    string(%);

    TODO: rewrite in this simpler form (present version is correct, but
    has more operations and looks messier):
    (C* cosh((3*C*t)/2)) / (sinh((3*C*t)/2)^(1/3)*sinh((3*C*t0)/2)^(2/3))
  */

  a_scale_factor_dot =
    exp(minus_one_third * log
        (sinh( 1.5 * background_cosm_params->H_0_sqrt_OmLam_0 *
               t_background ) /
         sinh( background_cosm_params->H_0_sqrt_OmLam_0_threehalves_t_0) )) *
    cosh(1.5 * background_cosm_params->H_0_sqrt_OmLam_0 *
         t_background) /
    sinh(background_cosm_params->H_0_sqrt_OmLam_0_threehalves_t_0) *
    background_cosm_params->H_0_sqrt_OmLam_0 *
    background_cosm_params->inhomog_a_scale_factor_now;
  return a_scale_factor_dot;
}



/*! \brief Calculates the second derivative of the scale factor of a flat FLRW
 * model.
 *
 * If \a recalculate_t_0 is set to 1 in background_cosm_params_s
 * structure, recalculates \f$ t_0 \f$ from the Hubble constant and sets
 * \a recalculate_t_0 to 0.
 *
 * If the model used is not a FLRW model and if
 * \a want_verbose parameter is set to 1, prints out an error.
 *
 * The scale factor is calculated from:
 * \f$ a(t) \propto \bigg(sinh \frac{3}{2} \sqrt{\frac{\Lambda}{3}} ct
 * \bigg)^{\frac{2}{3}} \f$
 * (Sahni & Starobinsky, 2000 \latexonly ,
 * \href{http://ned.ipac.caltech.edu/level5/March02/Sahni/Sahni3_2.html}
 * {IJMPD} \endlatexonly ).
 *
 * \param [in] background_cosm_params pointer to the
 * background_cosm_params_s containing relevant cosmological parameters
 * \param [in] t_background time
 * \param [in] want_verbose control parameter; defined and explained in
 * biscale_partition.c
 */
double a_ddot_flatFLRW(/* INPUTS: */
                       struct background_cosm_params_s * background_cosm_params,
                       double t_background,
                       int want_verbose
                       /* OUTPUTS: */
                       ){
  double a_scale_factor_ddot;
  /* TODO: good compilers should evaluate constants at compile time, so
     this is probably superfluous and distracting in the code */
  const double two_thirds = 2.0/3.0;
  double sinh_factor;

  if((1==background_cosm_params->EdS || !(background_cosm_params->flatFLRW) || background_cosm_params->Omm_0 > 1.0) && want_verbose){
    printf("t_flatFLRW Warning: called for invalid bg model.\n");
  };

  /* TODO: modularise this common section */
  if(background_cosm_params->recalculate_t_0){
    background_cosm_params->H_0_sqrt_OmLam_0 =
      background_cosm_params->H_0 * COSM_H_0_INV_GYR *
      sqrt(background_cosm_params->OmLam_0);

    background_cosm_params->t_0 = two_thirds *
      atanh(sqrt(background_cosm_params->OmLam_0))/
      background_cosm_params->H_0_sqrt_OmLam_0;

    background_cosm_params->H_0_sqrt_OmLam_0_threehalves_t_0 =
      background_cosm_params->H_0_sqrt_OmLam_0 * 1.5 *
      background_cosm_params->t_0;

    background_cosm_params->recalculate_t_0 = 0;
  };

  /*
    maxima, where the constant C = H_0 \sqrt(\Omega_{\Lambda 0})
    a : (sinh(3/2* C *t)/sinh(3/2* C *t0))^(2/3);
    diff(a,t,2), factor;
    trigsimp(%);
    string(%);

    (2*C^2 *sinh((3*C*t)/2)^2 - C^2) / ( 2*sinh((3*C*t)/2)^(4/3) *sinh((3*C*t0)/2)^(2/3) )

   */
  sinh_factor = sinh( 1.5 * background_cosm_params->H_0_sqrt_OmLam_0 *
                       t_background );

  a_scale_factor_ddot =
    (pow(background_cosm_params->H_0_sqrt_OmLam_0, 2) *
     (2.0* pow(sinh_factor,2) - 1.0)) /
    ( 2.0 * pow(sinh_factor, 4.0/3.0) *
      pow( sinh( background_cosm_params->H_0_sqrt_OmLam_0_threehalves_t_0 ), 2.0/3.0 )
      )
    *  background_cosm_params->inhomog_a_scale_factor_now;

  return a_scale_factor_ddot;
}
