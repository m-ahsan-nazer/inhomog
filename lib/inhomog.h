/*
   inhomog - tools and main program for backreaction calculations

   Copyright (C) 2012 Jan Ostrowski, Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! defined: Hamilton integrator; undefined: Raychaudhuri integrator */
/*!
#define INHOM_ENABLE_HAMILTON_INTEGRATOR 1
*/

/*! If INHOM_TURNAROUND_IS_COLLAPSE is defined, then the moment that a
   domain reaches turnaround, i.e. when \a \dot{a_D} drops to zero,
   will be treated as the moment of "collapse". This terminology is
   only intended for internal use in this program; proposals of better
   terminology are welcome. :) Obviously, turnaround is \b not really
   collapse.
*/
/*
#define INHOM_TURNAROUND_IS_COLLAPSE 1
*/

/*! If INHOM_OMEGA_R_D_RZA2_54 is defined, then the average scalar
    curvature and its "Omega" normalised version are calculated using
    RZA2 Equation (54). The alternative is to calculate OmR_D as the
    complement of the other Omega's using the Hamiltonian constraint
    (in which case the accuracy of satisfying the Hamiltonian
    constraint cannot be tested, since it is assumed). See RZA2.IV.B
    and Roukema & Ostrowski (2019) for more discussion.
*/
/*
#define INHOM_OMEGA_R_D_RZA2_54 1
*/

#include <gsl/gsl_spline.h>
#include <time.h>

/* #define TEST_GSL_MAX_N 1000000 */
#define TEST_GSL_MAX_N 1000000
#define TEST_GSL_N_BINS 1000
#define TEST_GSL_N_SIGMA 6
#define TEST_GSL_INTEG_ERROR 1e-5
#define TEST_GSL_N_CALLS 300000

#define HELP_LINE_TESTS "run a Test (\
i[nvariants], p[ower spec], \
u[niform])"
#define W_TYPE_DEFAULT 1
/* default thickness of spherical shells in Mpc */
#define DELTA_R_DEFAULT 0.05
#define HELP_LINE_WINDOW "Window function (0=BKS00_Tab1, 1=sphere, 2=shell) [1]"

#define TOL_LENGTH 1e-100
#define TOL_LENGTH_SQUARED 1e-200
#define TOL_TK_FACTOR 1e-300
#define TOL_LENGTH_INV_III 1e-75
#define TOL_ADOT_INITIAL_SQUARED 1e-100
#define TOL_RAY_FIRST_ORDER_MIN 1e-20
#define TOL_RAY_FIRST_ORDER_MAX 1e6
#define TOL_PK_EISHU 1e-50
#define TOL_MIN_LN_EISHU -100.0

/* tolerances, limits that indicate scale factors growing to extremely
   unphysical values */
#define TOL_A_D_MIN 1e-30
#define TOL_ADOT_SQ_A_OMD 1e-30
#define MAX_DEL_A_D_ACCEPTABLE 1000.0



/* definition 0 = BKS00 (C20)-(C22) = WRONG*/
/* #define  INHOM_INV_III_DEFN 0 */
/* definition 1 = (C9), (C10) + octave + maxima for (C17) generalisation = Roukema + Wiegand */
/* definition 2 = 1/27 <I>^3  (cf spherical) NAIVE */
#define INHOM_INV_III_DEFN 1

/* ****** Choice of a_D_dot_initial and integrator ***** */
/* leave INHOM_A_D_DOT_INITIAL_BKS undefined if you want HAM initial conditions */

/* #define INHOM_A_D_DOT_INITIAL_BKS 1 */

#ifndef INHOM_A_D_DOT_INITIAL_BKS
#define INHOM_A_D_DOT_INITIAL_HAM 1
#endif /* HAM case of initial conditions */

/* set INHOM_A_D_DOT_INITIAL_CURV_FACT to either 1 (GR case)
   or 0 (Newtonian, no curvature) */
#ifndef INHOM_A_D_DOT_INITIAL_CURV_FACTOR
#define INHOM_A_D_DOT_INITIAL_CURV_FACTOR 1
#endif


/* for invariants I, II, III */
double flat_length_3(double k_array[3]);

double flat_length_3_3params(double k_array0,
                             double k_array1,
                             double k_array2);

/* for invariants II, III */
double flat_dot_product_3(double *k_array,
                          int j_1,  /* which vector?
                                      j1 \in 1, 2, 3 */
                          int j_2   /* which vector?
                                      j2 \in 1, 2, 3 */
                          );

double flat_dot_product_3_6params
( double a1, double a2, double a3,
  double b1, double b2, double b3);



/* fundamental constants useful in FLRW cosmology (from
   cosmdist-0.2.4.13.2, (c) 2004, B Roukema GPL v2 or later) */

/*  c/H_0  expressed in units of h^{-1} Mpc, where h = H_0/(100 km/s/Mpc)
    In other words, it's   c  in km/s  divided by 100 km/s/Mpc  .
 */
#define COSM_C_ON_H_0_MPC  2997.92458

/*
       COSM_H_0_INV_GYR = 1 kms^-1 Mpc^-1 * 3.15576e16 s Gyr^-1
       = 10e3 m s^-1 (3.08568e22 m)^-1  * 3.15576e16 s Gyr^-1
       = 0.00102271 Gyr^-1

       This converts conventional H_0 units to inverse Gigayears.
*/
#define COSM_H_0_INV_GYR   1.02271e-3

/* multiply H(t) in 1/Gyr by this to get H(t) in km/s/Mpc */
#define KMS_PER_MPCGYR 977.79

#define BOUNDS_CHECK_H_0_MIN 10.0
#define BOUNDS_CHECK_H_0_MAX 600.0
#define BOUNDS_CHECK_Omm_0_MIN -0.01
#define BOUNDS_CHECK_Omm_0_MAX 10.0

struct background_cosm_params_s
{
  /* TODO: for the moment not much more than a dummy */
  int EdS;  /* 1 = EdS; 0 = other */
  int flatFLRW; /* 1 = flat FLRW; 0 = other */
  double H_0;  /* present-day Hubble parameter = Hubble constant = units = km/s/Mpc */
  double Omm_0;  /* present-day matter density parameter */
  double OmLam_0;  /* present-day background-dark-energy density parameter */
  double Ombary_0; /* present-day baryon density parameter */
  double Theta2p7; /* observed CMB temp / 2.7 K */
  double sigma8; /* rms of delta at t_0 of top hat 8 Mpc/h perturbation */
  double correct_Pk_norm; /* correction factor to multiply by
                             INHOMOG_PK_BKS_AMPLITUDE_RAW in order
                             to match sigma8 for the chosen power
                             spectrum and window function
                          */
  int correct_Pk_norm_known; /* is correct_Pk_norm already known? */
  /*
    double Omrad_0 = 0.0; */ /* present-day radiation density parameter */
  /* double wDE_0 = -1.0; */ /* present-day DE evolution parameter; -1.0 means a cosmological constant */
  int    recalculate_t_0; /* boolean: tell a function to make its own
                             calculation of t_0? otherwise: assume
                             that it's already calculated */

  double inhomog_a_scale_factor_initial;
  double inhomog_a_d_scale_factor_initial;
  double inhomog_a_scale_factor_now;

  /* DERIVED parameters; re-calculate if(recalculate_t_0) */
  double t_0;  /* background universe present age in Gyr */
  double H_0_sqrt_OmLam_0; /* in units of inverse Gyr */
  double H_0_sqrt_OmLam_0_threehalves_t_0; /* dimensionless */
};

struct precalculated_invariants_s
{
  int enabled;
  double inv_I;
  double inv_II;
  double inv_III;
};

struct sigma_sq_inv_triple_s
{
  int I_known; /* 1 prevents re-calculation; 0 allows re-calculation */
  int II_known;
  int III_known;
  double sqrt_E_sigma_sq_I; /* 'E' means expectation value; see BKS00 */
  double sqrt_E_sigma_sq_II;
  double sqrt_E_sigma_sq_III;
};


/* parameters for BKS00 integrands (C14), (C18), (C20)-(C22) */
struct rza_integrand_params_s
{
  /* window type */
  int    w_type;
  /* interior of sphere */
  double R_domain;
  /* spherical shell */
  double delta_R;
  double R_domain_1;
  double R_domain_2;
  /* power spectrum type ('B' = BBKS,
     'e' = Eisenstein & Hu short, 'E' = E&Hu full) */
  char pow_spec_type;

  /* If some or all of a triple of expectation values of the squares
     of the invariants have already been calculated, then store these
     values, so that they are availebl to calling routines if needed.
  */
  struct sigma_sq_inv_triple_s sigma_sq_inv_triple;

  /* The domain shape and size and power spectrum are
     not needed if a precalculated (e.g. externally calculated)
     triple of invariants is used instead. */
  struct precalculated_invariants_s precalculated_invariants;

  /* TODO: the following comment is probably obsolete:
     the background cosm parameters struct will normally be written
     to this structure by kinematic_backreaction() */
  struct background_cosm_params_s background_cosm_params;
};


/* Correct the P(k) normalisation */
int correct_Pk_normalisation(/* INPUTS: */
                             struct rza_integrand_params_s *rza_integrand_params, /* +OUT */
                             struct background_cosm_params_s *background_cosm_params, /* +OUT */
                             int want_verbose,
                             long   n_calls_invariants /* for invariant I integration */
                             );


/* initial version of P(k): hardwired BBKS/BKS H_0 = 50km/s/Mpc values */
double power_spectrum_flatspace(double k_mod, /* INPUTS */
                                double a_scale_factor_initial,
                                char   pow_spec_type,
                                struct background_cosm_params_s background_cosm_params
                                );

double power_spectrum_flatspace_BBKS
(double k_mod, /* INPUTS */
 double a_scale_factor_initial,
 struct background_cosm_params_s background_cosm_params
 );

double power_spectrum_flatspace_EisHu_short
(double k_mod, /* INPUTS */
 double a_scale_factor_initial,
 struct background_cosm_params_s background_cosm_params
 );

/*  the GCC compile option -finline-functions should inline the following */
double Transfer0tilde_EisHu(/* double k_mod_t0, -> passed via q_EH98 */
                            double alpha_c,
                            double beta_c,
                            double q_EH98);

double power_spectrum_flatspace_EisHu_full
(double k_mod, /* INPUTS */
 double a_scale_factor_initial,
 struct background_cosm_params_s background_cosm_params
 );

double window_R_k(double R_domain, double k_mod);

double window_R1_R2_k(double R_domain_1,    /* INPUTS: */
                      double R_domain_2,
                      double k_mod
                      );

#define DIM_BKS00 ((size_t)3)

/* k integration bounds @z=200 (?)
   octave: 2*pi ./ [0.0002 0.02 5 50]
   2pi / 0.02 Mpc = 314.16 Mpc^-1
   2pi / 0.0002 Mpc = 31416. Mpc^-1   low R, high k limit
*/
#define K_LOWER_BKS00 -31416.0
#define K_UPPER_BKS00 31416.0

/*
  Either INHOMOG_STANDARD_SCALE_FACTOR_CONVENTION
  or INHOMOG_BKS00_SCALE_FACTOR_CONVENTION
  can be defined, but not both.
*/

/* check that at least one is undefined */
#if defined (INHOMOG_STANDARD_SCALE_FACTOR_CONVENTION) && defined (INHOMOG_BKS00_SCALE_FACTOR_CONVENTION)
#error "Error. Both INHOMOG_STANDARD_SCALE_FACTOR_CONVENTION and INHOMOG_BKS00_SCALE_FACTOR_CONVENTION are defined."
#endif

/* if neither are defined, then choose a default: */
#if ! defined (INHOMOG_STANDARD_SCALE_FACTOR_CONVENTION) && ! defined (INHOMOG_BKS00_SCALE_FACTOR_CONVENTION)
/* DEFAULT: standard scale factor convention a(t_0) = 1 */
#define INHOMOG_STANDARD_SCALE_FACTOR_CONVENTION 1
#endif

/* if BKS00 scale factor convention: a(t_{initial}) = 1 */
#ifdef INHOMOG_BKS00_SCALE_FACTOR_CONVENTION
#undef INHOMOG_STANDARD_SCALE_FACTOR_CONVENTION
#endif


#ifdef INHOMOG_STANDARD_SCALE_FACTOR_CONVENTION
#define INHOMOG_A_SCALE_FACTOR_INITIAL (1.0/201.0)
#define INHOMOG_A_D_SCALE_FACTOR_INITIAL (1.0/201.0)
/*
#define INHOMOG_A_SCALE_FACTOR_INITIAL (1.0/35.819)
#define INHOMOG_A_D_SCALE_FACTOR_INITIAL (1.0/35.819)
*/
#define INHOMOG_A_SCALE_FACTOR_NOW (1.0)
#endif

#ifdef INHOMOG_BKS00_SCALE_FACTOR_CONVENTION
#define INHOMOG_A_SCALE_FACTOR_INITIAL (1.0)
#define INHOMOG_A_D_SCALE_FACTOR_INITIAL (1.0)
#define INHOMOG_A_SCALE_FACTOR_NOW (201.0)
#endif

/* #define INHOMOG_A_D_SCALE_FACTOR_NOW (1.0)  */
/* #define INHOMOG_Z_PLUS1_FACTOR_INITIAL 201.0 */
/* TODO: more general BBKS86 version, or more modern version */


double sigma_sq_invariant_I_integrand(double * k_array,
                                      size_t dim,
                                      void * params);

double sigma_sq_invariant_II_integrand(double * k_array,
                                      size_t dim,
                                      void * params);

double sigma_sq_invariant_III_integrand(double * k_array,
                                      size_t dim,
                                      void * params);

int test_power_spectrum_flatspace(int want_verbose);

int sigma_sq_invariant_I(/* INPUTS; */
                         struct rza_integrand_params_s rza_integrand_params,
                         long   n_calls,  /* number of times to evaluate the function */
                         int want_verbose,
                         double *the_integral, /* OUTPUTS: */
                         double *integ_error
                         );

int sigma_sq_invariant_II(/* INPUTS; */
                          struct rza_integrand_params_s rza_integrand_params,
                          long   n_calls,  /* number of times to evaluate the function */
                          int want_verbose,
                          double *the_integral, /* OUTPUTS: */
                          double *integ_error
                          );

int sigma_sq_invariant_III(/* INPUTS; */
                           struct rza_integrand_params_s rza_integrand_params,
                           long   n_calls,  /* number of times to evaluate the function */
                           int want_verbose,
                           double *the_integral, /* OUTPUTS: */
                           double *integ_error
                           );


int test_sigma_sq_invariants(/* INPUTS: */
                             struct rza_integrand_params_s rza_integrand_params
                             );



double a_EdS(/* INPUTS: */
             struct background_cosm_params_s * background_cosm_params,
             double t_background,
             int want_verbose
             /* OUTPUTS: */
             );

double t_EdS(/* INPUTS: */
             struct background_cosm_params_s * background_cosm_params,
             double a_scale_factor,
             int want_verbose
             /* OUTPUTS: */
             );

double a_dot_EdS(/* INPUTS: */
                 struct background_cosm_params_s * background_cosm_params,
                 double t_background,
                 int want_verbose
                 /* OUTPUTS: */
                 );

double a_ddot_EdS(/* INPUTS: */
                 struct background_cosm_params_s * background_cosm_params,
                 double t_background,
                 int want_verbose
                 /* OUTPUTS: */
                 );


double a_flatFLRW(/* INPUTS: */
                  struct background_cosm_params_s * background_cosm_params,
                  double t_background,
                  int want_verbose
                  /* OUTPUTS: */
                  );

double t_flatFLRW(/* INPUTS: */
                  struct background_cosm_params_s * background_cosm_params,
                  double a_scale_factor,
                  int want_verbose
                  /* OUTPUTS: */
                  );

double a_dot_flatFLRW(/* INPUTS: */
                      struct background_cosm_params_s * background_cosm_params,
                      double t_background,
                      int want_verbose
                      /* OUTPUTS: */
                      );

double a_ddot_flatFLRW(/* INPUTS: */
                      struct background_cosm_params_s * background_cosm_params,
                      double t_background,
                      int want_verbose
                      /* OUTPUTS: */
                      );


/* See (49) of Buchert et al RZA2 preprint v20120722 for the formula used
   here. */
int kinematical_backreaction(/* INPUTS: */
                              struct rza_integrand_params_s *rza_integrand_params,
                              struct background_cosm_params_s background_cosm_params,
                              double *t_background,
                              int    n_t_background,
                              double n_sigma[3],
                              long   n_calls_invariants,  /* for invariant I integration */
                              int want_planar, /* cf RZA2 V.A */
                              int want_spherical, /* cf RZA2 V.B.3 */
                              int want_verbose,
                              /* OUTPUTS: */
                              double *rza_Q_D
                              );


/*
  int test_kinematical_backreaction(
*/ /* INPUTS: */
/*
                                struct rza_integrand_params_s rza_integrand_params,
                                int want_verbose
                                );
*/


int curvature_backreaction(/* INPUTS: */
                           struct rza_integrand_params_s *rza_integrand_params,
                           struct background_cosm_params_s background_cosm_params,
                           double *t_background,
                           int    n_t_background,
                           double n_sigma[3],
                           long   n_calls_invariants,  /* for invariant I integration */
                           int want_planar, /* cf RZA2 V.A */
                           int want_spherical, /* cf RZA2 V.B.3 */
                           int want_verbose,
                           /* OUTPUTS: */
                           double *rza_R_D
                           );


struct a_D_RZA2_92_params_s
{
  double dummy;
};


/* initial verstion: test GSL suggested example */
#define N_INTEGRATORS 7
int test_inhomog_ODE(int want_verbose);


struct a_D_integrator_params_s
{
  /* constant for a_D integration; EdS: 8/(3t_i)^2 (1 - inv_I_initial) */
  double Aconst;
  /* gsl interpolator objects */
  gsl_interp_accel * accel_Q_D;
  gsl_spline * spline_Q_D;
  /* gsl interpolator objects for Hamiltonian case only */
  gsl_interp_accel * accel_R_D;
  gsl_spline * spline_R_D;
  /* gsl interpolator objects for Hamiltonian case only */
  gsl_interp_accel * accel_a_D;
  gsl_spline * spline_a_D;
  /* a_D_dot sign */
  /* int a_D_dot_sign; */ /* -1 or +1 */
  double t_maybe_neg_sqrt;
  double t_neg_sqrt;
  double a_D_dot_squared; /* return value used for searching for a zero of a_D_dot_squared */
};

/* find the collapse time after running scale_factor_D */
int find_collapse_time(/* INPUTS */
                       int  n_t_background,
                       double *a_D,
                       double *dot_a_D,
                       /* OUTPUTS */
                       int  *i_t_collapse,
                       int  *collapsed /* boolean */
                       );

/* scale_factor_D - wrapper for Raychaudhuri or Hamiltonian ODE integration of a_D */

int scale_factor_D(/* INPUTS: */
                   struct rza_integrand_params_s *rza_integrand_params,
                   struct background_cosm_params_s background_cosm_params,
                   double *t_background,
                   int  n_t_background,
                   double n_sigma[3],
                   long   n_calls_invariants,  /* for invariant I integration */
                   int want_planar, /* cf RZA2 V.A */
                   int want_spherical, /* cf RZA2 V.B.3 */
                   int want_verbose,
                   /* OUTPUTS: */
                   double *a_D,
                   double *dot_a_D,
                   int *unphysical
                   );


int scale_factor_D_Ray_ODE_func(double t,
                            const double func_first_order[],
                            double func_first_order_deriv[],
                            void *params);

int scale_factor_D_Ray_ODE_jacob(double t,
                             const double func_first_order[],
                             double *dfunc1D_partials,
                             double dfunc1D_dt[],
                             void *params);


int scale_factor_D_Ray(/* INPUTS: */
                   struct rza_integrand_params_s *rza_integrand_params,
                   struct background_cosm_params_s background_cosm_params,
                   double *t_background,
                   int  n_t_background,
                   double n_sigma[3],
                   long   n_calls_invariants,  /* for invariant I integration */
                   int want_planar, /* cf RZA2 V.A */
                   int want_spherical, /* cf RZA2 V.B.3 */
                   int want_verbose,
                   /* OUTPUTS: */
                   double *a_D,
                   double *dot_a_D,
                   int *unphysical
                   );

#define HAM_A_D_SQUARED_NEG_LIMIT 1e-2
#define HAM_A_D_SQUARED_ZERO_LIMIT 1e-20
#define SCALE_FACTOR_A_D_NEARLY_ZERO 1e-9


int scale_factor_D_Ham_ODE_func(double t,
                            const double func_first_order[],
                            double func_first_order_deriv[],
                            void *params);

int scale_factor_D_Ham_ODE_jacob(double t,
                             const double func_first_order[],
                             double *dfunc1D_partials,
                             double dfunc1D_dt[],
                             void *params);

/* just one run of integrating a_D from RZA2 (54), possibly
   recalculating t_neg_sqrt */
int scale_factor_D_Ham_one_try(/* INPUTS: */
                               struct a_D_integrator_params_s *a_D_integrator_params,
                               double t_ODE,
                               double h_ODE, /* step size */
                               double a_D_0,
                               double *t_background_in,
                               int  n_t_background_in,
                               int  recalculate_t_neg_sqrt,
                               double t_neg_sqrt_input,
                               /* OUTPUTS: */
                               double *t_neg_sqrt_output,
                               double *a_D_try,
                               double *dot_a_D_try
                               );


int scale_factor_D_Ham(/* INPUTS: */
                   struct rza_integrand_params_s *rza_integrand_params,
                   struct background_cosm_params_s background_cosm_params,
                   double *t_background,
                   int  n_t_background,
                   double n_sigma[3],
                   long   n_calls_invariants,  /* for invariant I integration */
                   int want_planar, /* cf RZA2 V.A */
                   int want_spherical, /* cf RZA2 V.B.3 */
                   int want_verbose,
                   /* OUTPUTS: */
                   double *a_D,
                   double *dot_a_D,
                   int *unphysical
                   );


int Omega_D(/* INPUTS: */
            struct rza_integrand_params_s *rza_integrand_params, /* +OUT */
            struct background_cosm_params_s background_cosm_params,
            double *t_background_in,
            int  n_t_background_in,
            double n_sigma[3],
            long   n_calls_invariants,  /* for invariant I integration */
            int want_planar, /* cf RZA2 V.A */
            int want_spherical, /* cf RZA2 V.B.3 */
            int want_verbose,
            /* OUTPUTS: */
            double *rza_Q_D,
            double *rza_R_D,
            double *a_D,  /* all of size n_t_background_in */
            double *dot_a_D,
            int *unphysical,
            double *H_D,
            double *Omm_D,
            double *OmQ_D,
            double *OmLam_D,
            double *OmR_D
            );


/* to be called from fortran with: bind(C,name='Omega_D_precalc') */
void Omega_D_precalc(/* INPUTS */
                      int64_t *scal_av_n_gridsize1,
                      int64_t *scal_av_n_gridsize2,
                      int64_t *scal_av_n_gridsize3,
                      double **I_inv,
                      double **II_inv,
                      double **III_inv,
                      /* aD_norm: proportional to initial volumes^(1/3) */
                      double **aD_norm,
                      int64_t *n_t_inhomog,
                      double *a_FLRW_init,
                      double *H1bg,
                      /* OUTPUTS */
                      double **t_RZA,
                      double **a_D_RZA
                      );

double growth_FLRW_func(
                        double t_background, void * params);

double dot_growth_FLRW_func(
                        double t_background, void * params);

/* growth function time derivative */
double dot_growth_FLRW(/* INPUTS: */
                       struct background_cosm_params_s * background_cosm_params,
                       double t_background,
                       int want_verbose
                       /* OUTPUTS: */
                       );

/* growth function for direct use */
double growth_FLRW(/* INPUTS: */
                   struct background_cosm_params_s * background_cosm_params,
                   double t_background,
                   int want_verbose
                   /* OUTPUTS: */
                   );

double ddot_growth_FLRW(/* INPUTS: */
                        struct background_cosm_params_s * background_cosm_params,
                        double t_background,
                        int want_verbose
                        /* OUTPUTS: */
                        );

double print_benchmark(clock_t b0, clock_t b1);


/* ***********************************************************
   Prototypes for deprecated routines

 * ***********************************************************
*/

/* To be called from fortran provided that the underscore _ convention
   matches; this is only weakly portable. This old version does not
   allow unequal scal_av_n_gridsize(1|2|3) values, and does not
   include normalisation of the initial domains in the case that they
   are of unequal volumes. So use of Omega_D_precalc() is strongly
   preferred. */
void omega_d_precalc_(/* INPUTS */
                      int64_t *scal_av_n_gridsize1,
                      double **I_inv,
                      double **II_inv,
                      double **III_inv,
                      int64_t *n_t_inhomog,
                      double *a_FLRW_init,
                      double *H1bg,
                      /* OUTPUTS */
                      double **t_RZA,
                      double **a_D_RZA
                      );
