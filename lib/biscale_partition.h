/*
   biscale_partition - illustrate VQZA with T3 = two domains - header file

   Copyright (C) 2017 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#define N_COLLAPSE_TYPES 4
enum collapse_types {
  planar_collapse, /* in reality, this is a superset of planar collapse */
  spherical_expansion, /* possibly also a superset of spherical
                          case; the collapsing domain does not
                          satisfy the "spherical" criterion */
  III_zero,
  II_zero
};

/* number of partitions in a biscale partition */
#define N_TWO 2
/* number of output times in the FLRW foliation */
#define N_TIMES 1000

int biscale_partition(/* INPUTS */
                      enum collapse_types collapse_type,
                      int want_verbose,
                      /* INPUTS/OUTPUTS: initial invariants, values */
                      double I_inv[N_TWO],
                      double II_inv[N_TWO],
                      double III_inv[N_TWO],
                      /* OUTPUTS */
                      double t_background_1cosm[N_TIMES],
                      double a_FLRW[N_TIMES],
                      double a_global_QZA[N_TIMES],
                      double a_D[N_TWO][N_TIMES], /* a_D local QZA */
                      double a_D_Newt[N_TWO][N_TIMES] /* a_D local Newt */
                      );
