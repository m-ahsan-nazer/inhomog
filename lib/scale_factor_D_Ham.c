/*
   scale_factor_D_Ham - based on (10) of Buchert et al RZA2 arXiv:1303.6193v2

   Copyright (C) 2013-2015 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file scale_factor_D_Ham.c */

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_fit.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif

#include "lib/inhomog.h"

#define DEBUG 1

#undef DEBUG

/* isolate Q and/or Lambda effect in integrating ODE */
/*
DISABLE_Q disables adding Q in the integration of the ODE;
DISABLE_LAMBDA_IN_ODE disables adding Lambda in the integration of the
ODE (without modifying other roles of Lambda such as in the
FLRW a(t), \dot{a}(t) relations).
*/

/*! \brief Stores first derivatives for the ODE system routine.
 *
 * First of two necessary functions for the ODE system. Creates a
 * \a func_first_order_deriv array, which stores entries necessary for
 * later procedures (depending on \a t, \f$ a_D \f$ and \a params).
 * Calculation uses Hamiltonian approach (based on Buchert et al. 2013
 * \latexonly \href{https://arxiv.org/abs/1303.6193}{arXiv: 1303.6193}
 * \endlatexonly ).
 *
 * If the \b DEBUG macro is set, prints out the values of the \a params
 * structure and \f$ (\dot{a_{D}})^2 \f$. If the latter is smaller than
 * \b -HAM_A_D_SQUARED_NEG_LIMIT, prints out time \a t, \f$
 * (\dot{a_{D}})^2 \f$ and Hubble parameter \f$ H_D \f$ and returns
 * \b GSL_EBADFUNC.
 *
 * Initially a void parameter, \a params later becomes a structure of
 * a_D_integrator_params_s type.
 *
 * The function also uses GSL cubic spline interpolation libraries to
 * calculate \a \f$Q_D \f$ (kinematical backreaction) and \a \f$R_D \f$
 * (Ricci scalar; curvature parameter) needed for the second \a
 * func_first_order_deriv entry.
 *
 * If successful, returns \b GSL_SUCCESS.
 *
 * \param [in] t time parameter (also used for the spline interpolation
 * of \f$ Q_D \f$ and \f$ R_D \f$)
 * \param [in] func_first_order an array storing a_D and its derivative [?]
 * \param [in] params a pointer to the void parameter
 * \param [out] func_first_order_deriv an array storing derivatives for
 * the ODE routine
 */
int scale_factor_D_Ham_ODE_func(double t,
                                const double func_first_order[],
                                double func_first_order_deriv[],
                                void *params){
  /*   double mu = *(double *)params; */
  struct a_D_integrator_params_s a_D_integrator_params =
    *(struct a_D_integrator_params_s *)params;
  /* const double one_third = 1.0/3.0; */
  const double one_sixth = 1.0/6.0;
  const double two_thirds = 2.0/3.0;

  double a_D_dot_squared;

  a_D_dot_squared =
    two_thirds * a_D_integrator_params.Aconst /
    func_first_order[0] -
    one_sixth * func_first_order[0]*func_first_order[0] *
    (
     gsl_spline_eval(a_D_integrator_params.spline_Q_D, /* Q_D */
                     t,
                     a_D_integrator_params.accel_Q_D) +
     gsl_spline_eval(a_D_integrator_params.spline_R_D, /* R_D */
                     t,
                     a_D_integrator_params.accel_R_D)
     );


  /* WARNING: Side effect of function! */
  /* use this for searching for a zero of a_D_dot_squared: */
  /*  a_D_integrator_params.a_D_dot_squared = a_D_dot_squared; */
  ((struct a_D_integrator_params_s *)params)->a_D_dot_squared = a_D_dot_squared;
#ifdef DEBUG
  printf("func: &a_D_integrator_params = %p ... a_D_integrator_params.a_D_dot_squared = %g\n",
         params,
         ((struct a_D_integrator_params_s *)params)->a_D_dot_squared);
#endif

  if(a_D_dot_squared < -HAM_A_D_SQUARED_NEG_LIMIT){

#ifdef DEBUG
    printf("scale_factor_D_Ham_ODE_func: t= %g, a_D_dot_squared = %g < 0, H_D sq = %g\n",
           t,
           a_D_dot_squared,
           a_D_dot_squared/(func_first_order[0]*func_first_order[0])
           );
#endif
    return GSL_EBADFUNC;
  };

  func_first_order_deriv[0] =
    sqrt(fmax(HAM_A_D_SQUARED_ZERO_LIMIT, a_D_dot_squared));


  if(t >  a_D_integrator_params.t_neg_sqrt){
    func_first_order_deriv[0] *= -1;
  };



  return GSL_SUCCESS;
}

/*! \brief Stores the vector of the derivative elements and the Jacobian
 * matrix.
 *
 * Second of two necessary functions for the ODE system. Stores the
 * Jacobian matrix in \a dfunc1d_partials and the time derivatives
 * vector in \a dfunc1D_dt.
 *
 * Initially a void parameter, \a params later becomes a structure of
 * a_D_integrator_params_s type.
 *
 * The Jacobian matrix is of the form:
 * \f$ \begin{bmatrix}
 * \frac{\partial{\dot{x}}}{\partial{x}} &
 * \frac{\partial{\dot{x}}}{\partial{\dot{x}}} \\[5pt]
 * \frac{\partial{\ddot{x}}}{\partial{x}} &
 * \frac{\partial{\ddot{x}}}{\partial{\dot{{x}}}}
 * \end{bmatrix}
 * \f$
 * and the time derivatives vector:
 * \f$ \big( \frac{\partial{\dot{x}}}{\partial{t}},
 *           \frac{\partial{\ddot{x}}}{\partial{t}} \big) \f$ .
 * [where are the 0,1, 1,0, 1,1 Jacobian entries? second entry of the
 * vector?]
 *
 * If successful, returns \b GSL_SUCCESS.
 *
 * \param [in] t time parameter
 * \param [in] func_first_order an array storing a_D and its derivative [?]
 * \param [in] params a pointer to the void parameter
 * \param [out] dfunc1D_partials Jacobian matrix
 * \param [out] dfunc1D_dt vector of the derivative elements
 */
int scale_factor_D_Ham_ODE_jacob(double t,
                             const double func_first_order[],
                             double *dfunc1D_partials,
                             double dfunc1D_dt[],
                             void *params){
  /*  double mu = *(double *)params; */
  struct a_D_integrator_params_s a_D_integrator_params =
    *(struct a_D_integrator_params_s *)params;
  const double one_third = 1.0/3.0;
  const double two_thirds = 2.0/3.0;
  const double one_sixth = 1.0/6.0;
  gsl_matrix_view dfunc1D_partials_mat =
    gsl_matrix_view_array(dfunc1D_partials,1,1);
  gsl_matrix * m = &dfunc1D_partials_mat.matrix;

  double a_D_dot_internal;
  double Q_R_internal;


  /* TODO: behaviour for negative square roots */
  Q_R_internal =
    gsl_spline_eval(a_D_integrator_params.spline_Q_D, /* Q_D */
                    t,
                    a_D_integrator_params.accel_Q_D) +
    gsl_spline_eval(a_D_integrator_params.spline_R_D, /* R_D */
                    t,
                    a_D_integrator_params.accel_R_D);

  a_D_dot_internal =
    sqrt(fmax(HAM_A_D_SQUARED_ZERO_LIMIT,
              (
               two_thirds * a_D_integrator_params.Aconst /
               func_first_order[0] -
               one_sixth * func_first_order[0]*func_first_order[0] *
               Q_R_internal )
              )
         );

  if(t >  a_D_integrator_params.t_neg_sqrt){
    a_D_dot_internal *= -1;
  };

  /* 0 = \dot{original x}; 1 = \dot{\dot{x}} */
  gsl_matrix_set (m, 0, 0,
                  0.5 / a_D_dot_internal *
                  ( -two_thirds * a_D_integrator_params.Aconst /
                    func_first_order[0]*func_first_order[0]
                    - one_third * func_first_order[0] * Q_R_internal )
                  ); /* \partial\dot{x}/\partial x */

  /* \partial\dot{x}/\partial t */
  dfunc1D_dt[0] = 0.5 / a_D_dot_internal *
    ( - one_sixth *
      func_first_order[0]*func_first_order[0] )
    *
    (gsl_spline_eval_deriv(a_D_integrator_params.spline_Q_D, /* Q_D */
                           t,
                           a_D_integrator_params.accel_Q_D) +
     gsl_spline_eval_deriv(a_D_integrator_params.spline_R_D, /* R_D */
                           t,
                           a_D_integrator_params.accel_R_D)
     );

  return GSL_SUCCESS;
}


/* Try to integrate a_D, either:
   - try to find a best estimate of t_neg_sqrt without prior info, or
   - use t_neg_sqrt_input without modification, or
   - use t_neg_sqrt_input but modify it first.
   The value used is output in all three cases.
   In the first case, integration will restart from a little earlier
   than the estimated value of t_neg_sqrt and then continue for all
   t_background_in values.
 */

/*! \brief Inner function for the scale factor \f$ a_D \f$ calculation.
 *
 * Solves the ordinary differential equation for \f$ a_D \f$ based on
 * the Hamilton equation and initial scale factor value
 * \f$ \dot{a}_{D0} \f$ = func_first_order[0].
 *
 * Needs to take into account the possibility of \f$ \dot{a}_D \f$ being
 * negative. Takes the initial time value for which \f$ \dot{a}_D \f$
 * becomes 0 (taken from the a_D_integrator_params_s structure:
 * \a t_neg_sqrt) and through the GSL linear fit routine finds a better
 * approximation of \a t_neg_sqrt. This happens only if the
 * \a recalculate_t_neg_sqrt parameter is set to 1; otherwise the
 * function simply sets \a t_neg_sqrt value to \a t_neq_sqrt_input.
 *
 * Uses the Runge-Kutta-Fehlberg (4, 5) integration method.
 *
 * If the DEBUG option is enabled, prints out the results during the
 * calculation for additional control over the integration process.
 *
 * If something went wrong at some particular time step \a i_t, sets the
 * \a a_D_try[i_t] value to \b SCALE_FACTOR_A_D_NEARLY_ZERO and its
 * derivative at \a i_t to 0.
 *
 * \param [in] rza_integrand_params_s pointer to the structure
 * containing parameters necessary for the ODE integration
 * \param [in] t_ODE integration time
 * \param [in] h_ODE integration step size
 * \param [in] a_D_0 initial value of the scale factor \f$ \dot{a_D0}\f$
 * (initial condition)
 * \param [in] t_background_in pointer to the matrix of time values
 * \param [in] n_t_background_in size of the \a t_background_in matrix
 * \param [in] recalculate_t_neg_sqrt control parameter; if set to 1,
 * recalculates \a t_neg_sqrt instead of taking the t_neg_sqrt_input
 * value
 * \param [in] t_neg_sqrt_input initial time for which \f$ \dot{a_D}
 * < 0 \f$ (worse approximation)
 * \param [out] t_neg_sqrt_output pointer to the final time for which
 * \f$ \dot{a_D} < 0 \f$ (better approximation)
 * \param [out] a_D_try pointer to the scale factor \f$ a_D \f$
 * \param [out] dot_a_D_try pointer to the first derivative of the scale
 * factor \f$ \dot{a_D} \f$
 */
int scale_factor_D_Ham_one_try(/* INPUTS: */
                               struct a_D_integrator_params_s *a_D_integrator_params,
                               double t_ODE,
                               double h_ODE, /* step size */
                               double a_D_0,
                               double *t_background_in,
                               int  n_t_background_in,
                               int  recalculate_t_neg_sqrt,
                               double t_neg_sqrt_input,
                               /* OUTPUTS: */
                               double *t_neg_sqrt_output,
                               double *a_D_try,
                               double *dot_a_D_try
                               ){
  gsl_odeiv2_step *s_ODE;
  gsl_odeiv2_control *c_ODE;
  gsl_odeiv2_evolve *e_ODE;

  const gsl_odeiv2_step_type *T_gsl_ODE_type =
      gsl_odeiv2_step_rkf45;

  /* parameters specific to ODE integrator */
  double func_first_order[1]; /* IC = initial conditions */

  double t_ODE_initial;
  double h_ODE_initial;
  double a_D_0_initial;

  /* a_D_dot trackers */
#define N_A_D_DOT_SQUARED 50
#define N_A_D_DOT_SQUARED_SIGN_CHANGE_MIN 5
#define N_A_D_POS_SECOND_DERIV_MIN 10
  double a_D_dot_squared[N_A_D_DOT_SQUARED];
  double t_tracker[N_A_D_DOT_SQUARED];
  double a_D_tracker[N_A_D_DOT_SQUARED];
  int    i_a_D_dot_squared;
  int    n_a_D_dot_squared;
  int    n_neg_square;
  int    n_pos_square;
  int    n_neg_second_deriv;
  int    n_pos_second_deriv;
  int    i_fit1,n_fit; /* indices for linear fitting around zero point */
  double c0_fit, c1_fit, cov00_fit, cov01_fit, cov11_fit, sumsq_fit;
  int    turnaround_found;

  gsl_odeiv2_system sys_ODE;

  int i_GSL_errno;

#ifdef DEBUG
  double a_D_dot_tmp;
#endif

  int i_t;

  sys_ODE.function = scale_factor_D_Ham_ODE_func;
  sys_ODE.jacobian = scale_factor_D_Ham_ODE_jacob;
  sys_ODE.dimension = 1;
  sys_ODE.params = a_D_integrator_params;

  s_ODE = gsl_odeiv2_step_alloc(T_gsl_ODE_type,1);
  /*  c_ODE = gsl_odeiv2_control_y_new(0.0,1e-6);  */
  c_ODE = gsl_odeiv2_control_standard_new(1e-6,1e-6,1.0,1.0);
  e_ODE = gsl_odeiv2_evolve_alloc(1);

  func_first_order[0] = a_D_0; /* initial condition */

  /* store the initial values for the case of calculating
     and using a new t_neg_sqrt estimate */
  t_ODE_initial = t_ODE;
  h_ODE_initial = h_ODE;
  a_D_0_initial = a_D_0;


  a_D_integrator_params->t_neg_sqrt = t_neg_sqrt_input;

#ifdef DEBUG
  printf("a_D_integrator_params->t_neg_sqrt  = %g\n",
         a_D_integrator_params->t_neg_sqrt);
#endif


  n_a_D_dot_squared=0;
  n_neg_square=0;
  n_pos_square=0;
  n_neg_second_deriv=0;
  n_pos_second_deriv=0;
  turnaround_found=0;

  i_GSL_errno = 0;
  for (i_t=0; i_t<n_t_background_in; i_t++){
    while (t_ODE < t_background_in[i_t] && 0==i_GSL_errno){
      /* don't allow bigger steps than 10% of t */
      h_ODE = fmin(1e-1*t_ODE , h_ODE);
      /* allow at least enough steps to search for negative a_D_dot_squared */
      if(i_t< n_t_background_in -1){
        h_ODE = fmin(h_ODE,
                     (t_background_in[i_t+1] - t_background_in[i_t])/
                     (double)(N_A_D_DOT_SQUARED_SIGN_CHANGE_MIN+1) );
      };

      i_GSL_errno = gsl_odeiv2_evolve_apply(e_ODE, c_ODE, s_ODE,
                                           &sys_ODE,
                                           &t_ODE,
                                           t_background_in[i_t],
                                           &h_ODE,
                                           func_first_order);

#ifdef DEBUG
      /* DEBUG */
      printf("t_ODE = %g cf %g; h_ODE = %g\n",
             t_ODE,t_background_in[i_t], h_ODE);
      if(i_GSL_errno) printf("scale_factor_D_Ham: %s\n",
                             gsl_strerror(i_GSL_errno));
      scale_factor_D_Ham_ODE_func(t_ODE,
                                  func_first_order,
                                  (double*)&a_D_dot_tmp,
                                  a_D_integrator_params);
      printf("a_D_dot_tmp = %g ; ",a_D_dot_tmp);
      printf("&a_D_integrator_params = %p  a_D_integrator_params.a_D_dot_squared = %g\n",
             (void *)a_D_integrator_params,
             a_D_integrator_params->a_D_dot_squared);

#endif

      /* track sign change in a_D_dot */
      if(recalculate_t_neg_sqrt && 0==turnaround_found){

        /* beginning: increase number of tracked points if necessary */
        if(n_a_D_dot_squared < N_A_D_DOT_SQUARED){
          n_a_D_dot_squared++;
          i_a_D_dot_squared = n_a_D_dot_squared-1;
          /* add the new value */
          t_tracker[i_a_D_dot_squared] =
            t_ODE;
          a_D_tracker[i_a_D_dot_squared] =
            func_first_order[0];
          a_D_dot_squared[i_a_D_dot_squared] =
            a_D_integrator_params->a_D_dot_squared;
#ifdef DEBUG
          printf("early tracker: a_D_integrator_params.a_D_dot_squared = %g\n",
                 a_D_integrator_params->a_D_dot_squared);
          printf("  n_neg_square  n_pos_square = %d %d\n",
                 n_neg_square,  n_pos_square);
#endif

          if(a_D_dot_squared[i_a_D_dot_squared] <= 0){
            n_neg_square++;
          }else{
            n_pos_square++;
          };
          if(i_a_D_dot_squared > 0){
            if(a_D_tracker[i_a_D_dot_squared-1] <= a_D_tracker[i_a_D_dot_squared]){
              n_neg_second_deriv++;
            }else{
              n_pos_second_deriv++;
            };
          };

#ifdef DEBUG
          printf("AA: t_tracker[%d] = %g %g n_pos/neg_square = %d %d\n",
                 i_a_D_dot_squared,
                 t_tracker[i_a_D_dot_squared],
                 a_D_dot_squared[i_a_D_dot_squared],
                 n_pos_square,
                 n_neg_square);
#endif


        }else{

          /* reduce the counts for neg or pos squares for the oldest
             calculation, if there is at least one old value */
          if(n_a_D_dot_squared-1 > 0){  /* should this be if(n_a_D_dot_squared > 0) ??? */
            if(a_D_dot_squared[0] <= 0){
              n_neg_square--;
            }else{
              n_pos_square--;
            };
            if(n_a_D_dot_squared-1 > 1){
              if(a_D_tracker[0] <= a_D_tracker[1]){
                n_neg_second_deriv--;
              }else{
                n_pos_second_deriv--;
              };
            };

          }else{
            printf("Please increase N_A_D_DOT_SQUARED or modify scale_factor_D_Ham.c.\n");
            exit(1);
          };

#ifdef DEBUG
          printf("BB: t_tracker[%d] = %g %g n_pos/neg_square = %d %d\n",
                 n_a_D_dot_squared-1,
                 t_tracker[n_a_D_dot_squared-1],
                 a_D_dot_squared[n_a_D_dot_squared-1],
                 n_pos_square,
                 n_neg_square);
#endif

          /* shift a_D_dot^2 tracker values to left */
          for(i_a_D_dot_squared=0;
              i_a_D_dot_squared < n_a_D_dot_squared;
              i_a_D_dot_squared++){
            if(i_a_D_dot_squared < n_a_D_dot_squared -1){
              t_tracker[i_a_D_dot_squared] =
                t_tracker[i_a_D_dot_squared+1];
              a_D_tracker[i_a_D_dot_squared] =
                a_D_tracker[i_a_D_dot_squared+1];
              a_D_dot_squared[i_a_D_dot_squared] =
                a_D_dot_squared[i_a_D_dot_squared+1];
            }else if(i_a_D_dot_squared == n_a_D_dot_squared -1){
              /* add the new value */
              t_tracker[i_a_D_dot_squared] =
                t_ODE;
              a_D_tracker[i_a_D_dot_squared] =
                func_first_order[0];
              a_D_dot_squared[i_a_D_dot_squared] =
                a_D_integrator_params->a_D_dot_squared;
#ifdef DEBUG
              printf("late tracker: a_D_integrator_params.a_D_dot_squared = %g\n",
                     a_D_integrator_params->a_D_dot_squared);
#endif

              if(a_D_dot_squared[i_a_D_dot_squared] <= 0){
                n_neg_square++;
              }else{
                n_pos_square++;
              };
              if(i_a_D_dot_squared > 0){
                if(a_D_tracker[i_a_D_dot_squared-1] <= a_D_tracker[i_a_D_dot_squared]){
                  n_neg_second_deriv++;
                }else{
                  n_pos_second_deriv++;
                };
              };
            };
          }; /*         for(i_a_D_dot_squared=0; ...) */
        }; /* if(n_a_D_dot_squared < N_A_D_DOT_SQUARED) */

#ifdef DEBUG
        printf("CC: t_tracker[%d] = %g %g n_pos/neg_square = %d %d\n",
               n_a_D_dot_squared-1,
               t_tracker[n_a_D_dot_squared-1],
               a_D_dot_squared[n_a_D_dot_squared-1],
               n_pos_square,
               n_neg_square);
#endif


        /* set the turnaround time if this has been found */
        if(n_neg_square > N_A_D_DOT_SQUARED_SIGN_CHANGE_MIN ||
           n_pos_second_deriv >  N_A_D_POS_SECOND_DERIV_MIN
           ){
#ifdef DEBUG
          printf("Searching for zero:\n");
#endif
          if(n_neg_square > N_A_D_DOT_SQUARED_SIGN_CHANGE_MIN){
            n_fit = 2*n_neg_square;
            i_fit1 = n_a_D_dot_squared - n_fit;
            if(i_fit1 < 0){
              i_fit1 = 0;
              n_fit = n_a_D_dot_squared;
            };

            gsl_fit_linear( (double *)&(t_tracker[i_fit1]),1,
                            (double *)&(a_D_dot_squared[i_fit1]),1,
                            (size_t)n_fit,
                            &c0_fit, &c1_fit,
                            &cov00_fit, &cov01_fit, &cov11_fit,
                            &sumsq_fit
                            );
            /*
              int gsl_fit_linear (const double * X, const size_t
              XSTRIDE, const double * Y, const size_t YSTRIDE, size_t N,
              double * C0, double * C1, double * COV00, double * COV01,
              double * COV11, double * SUMSQ)
            */
            if(fabs(c1_fit) < TOL_LENGTH_SQUARED){
              printf("ERROR: scale_factor_D_Ham zero slope for zero search, c0_fit, c1_fit = %g %g.\n",
                     c0_fit,c1_fit);
              exit(1);
            };

            a_D_integrator_params->t_neg_sqrt = -c0_fit/c1_fit;
          }else{
            if(!(n_pos_second_deriv >  N_A_D_POS_SECOND_DERIV_MIN)){
              printf("scale_factor_D_Ham: Programming error!\n");
              exit(1);
            };

            i_fit1= n_a_D_dot_squared - n_pos_second_deriv;
            if(i_fit1 < 0){
              i_fit1 = 0;
            };
            /* if the derivative became positive, then use a cruder estimate */
            a_D_integrator_params->t_neg_sqrt = t_tracker[i_fit1];
          };


          turnaround_found = 1;
#ifdef DEBUG
          printf("c0_fit, c1_fit, a_D_integrator_params.t_neg_sqrt= %g %g %g\n",
                 c0_fit, c1_fit,a_D_integrator_params->t_neg_sqrt);
#endif

          /* modify the output t_neg_sqrt value since this
             has been recalculated */
          *t_neg_sqrt_output = a_D_integrator_params->t_neg_sqrt;


          /* reset the ODE integrator structures and re-start from an earlier time */
          gsl_odeiv2_step_reset(s_ODE);
          gsl_odeiv2_evolve_reset(e_ODE);
          /*
             t_ODE = t_tracker[i_fit1];
             func_first_order[0] = a_D_tracker[i_fit1];
          */
          t_ODE = t_ODE_initial;
          func_first_order[0] = a_D_0_initial;
          h_ODE = h_ODE_initial;

          n_a_D_dot_squared=0;
          n_neg_square=0;
          n_pos_square=0;
          n_neg_second_deriv=0;
          n_pos_second_deriv=0;

          /* aim: if a bad negative squared a_D_dot was found earlier,
             then forget it */
          i_GSL_errno = 0;
        }; /*       if(n_neg_square > N_A_D_DOT_SQUARED_SIGN_CHANGE_MIN) */
      }; /*       if(!turnaround_found) */

    }; /*  while (t_ODE < t_background_in[i_t]) */

    if(GSL_EBADFUNC!=i_GSL_errno){
      a_D_try[i_t] = func_first_order[0];

      scale_factor_D_Ham_ODE_func(t_background_in[i_t],
                                  (double*)&(a_D_try[i_t]),
                                  (double*)&(dot_a_D_try[i_t]),
                                  a_D_integrator_params);
    }else{
      a_D_try[i_t] = SCALE_FACTOR_A_D_NEARLY_ZERO;
      dot_a_D_try[i_t] = 0.0;
    };
  };

  gsl_odeiv2_step_free (s_ODE);
  gsl_odeiv2_evolve_free (e_ODE);
  gsl_odeiv2_control_free (c_ODE);

  return 0;
} /* int scale_factor_D_Ham_one_try */



/* #define MAX_N_TRY_A_D_HAM 100 */
#define MAX_N_TRY_A_D_HAM 100
/* #define halfmax_fraction 0.5 */
#define halfmax_fraction 0.1

/* TODO (EASY): The accuracy of finding the t_neg_sqrt value depends
   on the input choice of t_background_in values; about 100 values
   gives reasonable results as of 2015-02-15. This must be fixed
   to become independent of the calling routine!
*/

/*! \brief Calculates the scale factor \f$ a_D \f$ according to the
 * Hamiltonian evolution.
 *
 * Uses the inner ODE solver \ref scale_factor_D_Ham_one_try.
 *
 * Uses the Runge-Kutta-Fehlberg (4, 5) integration method.
 *
 * The initial time is set to one of two possible values; calculation
 * happens according to either t_EdS or t_flatFLRW function (both are
 * described in more detail in FLRW_background.c) and is controlled by
 * the t_EdS and t_flatFLRW parameters (for a more detailed description
 * see biscale_partition.c). An error message is shown if neither of
 * them is set to 1.
 *
 * Uses kinematical_backreaction function to calculate \f$ Q_D \f$. For
 * a more detailed description see kinematical_backreaction.c .
 *
 * Uses curvature_backreaction function to calculate \f$ R_D \f$. For a
 * more detailed description see curvature_backreaction.c .
 *
 * Allows disabling of \f$ Q_D \f$ and/or \f$ \Lambda \f$ during the
 * calculation by using \b DISABLE_Q and \b DISABLE_LAMBDA_IN_ODE macros
 * respectively.
 *
 * Initializes the scale_factor_D_Ham_one_try function twice; first to
 * calculate the first approximation, which allows \a t_neg_sqrt
 * recalculation, and then in a loop for further approximations. Number
 * of maximum estimates is defined by \b MAX_N_TRY_A_D_HAM.
 *
 * \param [in] rza_integrand_params_s structure containing parameters
 * necessary for the ODE integration
 * \param [in] background_cosm_params_s structure containing all
 * relevant cosmological parameters (defined outside this file)
 * \param [in] t_background_in pointer to the time values matrix
 * \param [in] n_t_background_in number of entries in the
 * \a t_background_in matrix
 * \param [in] n_sigma[3] an array with maximal numbers of standard
 * deviations for each invariant
 * \param [in] n_calls_invariants integration parameter for functions
 * sigma_sq_invariant_I and sigma_sq_invariant_II
 * \param [in] want_planar control parameter; defined in
 * biscale_partition.c
 * \param [in] want_spherical control parameter; defined in
 * biscale_partition.c
 * \param [in] want_verbose control parameter; defined in
 * biscale_partition.c
 * \param [out] a_D pointer to the scale factor
 * \param [out] dot_a_D pointer to the first derivative of a_D
 * \param [out] unphysical control parameter for checking if values are
 * physically reasonable
 */
int scale_factor_D_Ham(/* INPUTS: */
                   struct rza_integrand_params_s *rza_integrand_params,
                   struct background_cosm_params_s background_cosm_params,
                   double *t_background_in,
                   int  n_t_background_in,
                   double n_sigma[3],
                   long   n_calls_invariants,  /* for invariant I integration */
                   int want_planar, /* cf RZA2 V.A */
                   int want_spherical,
                   int want_verbose,
                   /* OUTPUTS: */
                   double *a_D,
                   double *dot_a_D,
                   int *unphysical
                   ){

  double *rza_Q_D;
  double *rza_Q_D_minus_twoLambda_FLRW;
  double *rza_R_D;
#define N_T_SPLINE 100000
  /* use external functions to calculate initial t, a, adot */
  double t_initial, a_initial, a_dot_initial;
  double Omm_initial;  /* calculate from Omm_0 for non-EdS case */
  double t_0,dt; /* locally calculate initial and final times */
  double t_background[N_T_SPLINE];
  int i_t;

#ifndef DISABLE_LAMBDA_IN_ODE
  double minus_two_Lambda_invGyr2; /* derived parameter: -2 * Lambda in units of Gyr^{-2}; to be calculated from OmLam_0 and H_0 */
#endif

  /* structure of constant and spline tables for integration */
  struct a_D_integrator_params_s a_D_integrator_params;
  double inv_I, inv_I_err;
  /* double inv_II, inv_II_err;
     double inv_III, inv_III_err; */

  double t_ODE;
  double h_ODE; /* step size */
  double a_D_0;

  double t_neg_sqrt_initial;
  double t_neg_sqrt_first_estimate;
  double t_neg_sqrt_list[MAX_N_TRY_A_D_HAM];
  /* declare a_D spline */
  gsl_interp_accel *accel_a_D = gsl_interp_accel_alloc();
  gsl_spline *spline_a_D;

  /* quality parameter for deciding when the best
     t_neg_sqrt estimate has been found */
  double dot_a_D_anomaly_sq[MAX_N_TRY_A_D_HAM];
  double dot_a_D_anomaly_sq_min;
  double delta_anomaly;
  int    i_t_neg_sqrt;
  int    i_t_low, i_t_high;
  double a_D_max;
  double n_intervals_t_try_d, delta_t_try;
  double t_neg_sqrt_best;

  double *a_D_try = NULL;
  double *dot_a_D_try = NULL;
  double dot_a_D_Ham_try = 0.0; /* the first try is unlikely to be the best one */
  double dot_a_D_spline_try;
  /* double dot_a_D_spline_try_old=9e9; */


  /* declare Q_D spline - in practice Q_D - 2 Lambda*/
  gsl_interp_accel *accel_Q_D = gsl_interp_accel_alloc();
  gsl_spline *spline_Q_D =
    gsl_spline_alloc(gsl_interp_cspline, N_T_SPLINE);

  /* declare R_D spline */
  gsl_interp_accel *accel_R_D = gsl_interp_accel_alloc();
  gsl_spline *spline_R_D =
    gsl_spline_alloc(gsl_interp_cspline, N_T_SPLINE);


  /* prepare for initialising Q_D estimate */
  rza_Q_D = malloc(N_T_SPLINE * sizeof(double));
  rza_Q_D_minus_twoLambda_FLRW = malloc(N_T_SPLINE * sizeof(double));

  /* prepare for initialising R_D estimate */
  rza_R_D = malloc(N_T_SPLINE * sizeof(double));


  /* initial time; check that input times are later */
  if(1 == background_cosm_params.EdS){
    t_initial = t_EdS(&background_cosm_params,
                      background_cosm_params.inhomog_a_scale_factor_initial,
                      want_verbose);
  }else if(1 == background_cosm_params.flatFLRW &&
           background_cosm_params.Omm_0 < 1.0){
    t_initial = t_flatFLRW(&background_cosm_params,
                           background_cosm_params.inhomog_a_scale_factor_initial,
                           want_verbose);
  }else{
      printf("No other options for background_cosm_params so far in program.\n");
      return 1;
  };

  if( gsl_stats_min(t_background_in, 1, (size_t)n_t_background_in)
      < t_initial ){
    printf("scale_factor_D_Ham ERROR: smallest t_background_in = %g = too small < %g\n",
           gsl_stats_min(t_background_in, 1, (size_t)n_t_background_in),
           t_initial);
    exit(1);
  };

  /* high number of times needed for calculating spline */
  t_0 = gsl_stats_max(t_background_in, 1, (size_t)n_t_background_in);
  dt= (t_0-t_initial)/(double)(N_T_SPLINE-1);
  for(i_t=0; i_t<N_T_SPLINE-1; i_t++){
    t_background[i_t] = t_initial + (double)i_t *dt;
  };
  t_background[N_T_SPLINE-1] = t_0;

  /* initialise Q_D estimate */
  kinematical_backreaction(rza_integrand_params,
                           background_cosm_params,
                           t_background, N_T_SPLINE,
                           n_sigma,
                           n_calls_invariants,
                           want_planar, /* cf RZA2 V.A */
                           want_spherical,
                           want_verbose,
                           rza_Q_D
                           );

  /* initialise R_D estimate */
  curvature_backreaction(rza_integrand_params,
                         background_cosm_params,
                         t_background, N_T_SPLINE,
                         n_sigma,
                         n_calls_invariants,
                         want_planar, /* cf RZA2 V.A */
                         want_spherical,
                         want_verbose,
                         rza_R_D
                         );

  if(1 == background_cosm_params.EdS){
    for(i_t=0; i_t<N_T_SPLINE; i_t++){
      rza_Q_D_minus_twoLambda_FLRW[i_t] = 0.0;

#ifndef DISABLE_Q
      rza_Q_D_minus_twoLambda_FLRW[i_t] += rza_Q_D[i_t];
#endif
    };
  }else if(1 == background_cosm_params.flatFLRW &&
           background_cosm_params.Omm_0 < 1.0){
#ifndef DISABLE_LAMBDA_IN_ODE
    minus_two_Lambda_invGyr2 =
      -6.0 * background_cosm_params.OmLam_0 *
      background_cosm_params.H_0 * COSM_H_0_INV_GYR *
      background_cosm_params.H_0 * COSM_H_0_INV_GYR ;
#endif

    for(i_t=0; i_t<N_T_SPLINE; i_t++){
      rza_Q_D_minus_twoLambda_FLRW[i_t] = 0.0;

#ifndef DISABLE_Q
      rza_Q_D_minus_twoLambda_FLRW[i_t] += rza_Q_D[i_t];
#endif

#ifndef DISABLE_LAMBDA_IN_ODE
      rza_Q_D_minus_twoLambda_FLRW[i_t] +=
        minus_two_Lambda_invGyr2;
#endif
    };
  }; /*   if(1 == background_cosm_params.EdS) */


  /* initialise Q_D spline */
  gsl_spline_init( spline_Q_D,
                   t_background,
                   rza_Q_D_minus_twoLambda_FLRW,
                   N_T_SPLINE );

  /* initialise R_D spline */
  gsl_spline_init( spline_R_D,
                   t_background,
                   rza_R_D,
                   N_T_SPLINE );

  rza_integrand_params->background_cosm_params =
    background_cosm_params; /* needed by the invariant integrators for P(k) */


  if(rza_integrand_params->sigma_sq_inv_triple.I_known){
    /* set the scale of inv_I */
    inv_I = n_sigma[0] *
      rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_I;
  }else{
    sigma_sq_invariant_I( *rza_integrand_params,
                          n_calls_invariants,
                          want_verbose,
                          &inv_I, &inv_I_err );
    /* set the scale of inv_I */
    rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_I =
      sqrt(inv_I);
    rza_integrand_params->sigma_sq_inv_triple.I_known = 1;
    inv_I = n_sigma[0] *
      rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_I;
  };

  if(want_verbose){
    printf("scale_factor_D_Ham (pre-integration): inv_I err = %g %g\n",
           inv_I,  fabs(n_sigma[0]) * 0.5*inv_I_err/inv_I);
  };



  if(1==background_cosm_params.EdS){
    a_initial =
      a_EdS(&background_cosm_params, t_initial, want_verbose);
    a_dot_initial =
      a_dot_EdS(&background_cosm_params, t_initial, want_verbose);
    a_D_integrator_params.Aconst = 1.5 *
      a_dot_initial*a_dot_initial /
      (a_initial*a_initial) *
      background_cosm_params.inhomog_a_d_scale_factor_initial *
      background_cosm_params.inhomog_a_d_scale_factor_initial *
      background_cosm_params.inhomog_a_d_scale_factor_initial *
      (1.0 - inv_I);
  }else if(1 == background_cosm_params.flatFLRW &&
           background_cosm_params.Omm_0 < 1.0){
    a_initial =
      a_flatFLRW(&background_cosm_params,
                 t_initial, want_verbose);
    a_dot_initial =
      a_dot_flatFLRW(&background_cosm_params,
                     t_initial, want_verbose);
    /* flat FLRW case, e.g. (1.1) of arXiv:1303.4444;
       assumes FLRW flatness!
     */
    Omm_initial = background_cosm_params.Omm_0 /
      ( exp(3.0*log(a_initial/
                    background_cosm_params.inhomog_a_scale_factor_now)) *
        (1.0 - background_cosm_params.Omm_0) +
        background_cosm_params.Omm_0 );

    a_D_integrator_params.Aconst = 1.5 *
      a_dot_initial*a_dot_initial /
      (a_initial*a_initial) *
      background_cosm_params.inhomog_a_d_scale_factor_initial *
      background_cosm_params.inhomog_a_d_scale_factor_initial *
      background_cosm_params.inhomog_a_d_scale_factor_initial *
      Omm_initial *
      (1.0 - inv_I);
  }else{
    printf("scale_factor_D_Ham. Background models other than EdS\n");
    printf("and flat FLRW not yet programmed.\n");
    return 1;
  };

  a_D_integrator_params.accel_Q_D = accel_Q_D;
  a_D_integrator_params.spline_Q_D = spline_Q_D;

  a_D_integrator_params.accel_R_D = accel_R_D;
  a_D_integrator_params.spline_R_D = spline_R_D;


  /* set up ODE parameters */
  h_ODE = 1e-6 *t_initial;
  /* *  a_D_initial  */
  t_ODE = t_initial;
  a_D_0 = background_cosm_params.inhomog_a_d_scale_factor_initial;

  /* assume initial expansion (not contraction), i.e. that a turnaround
     epoch has not yet been found: */
  t_neg_sqrt_initial = t_background_in[n_t_background_in] + 9e19;

  a_D_try = malloc((size_t)n_t_background_in * sizeof(double));
  dot_a_D_try = malloc((size_t)n_t_background_in * sizeof(double));
  if(NULL==a_D_try || NULL==dot_a_D_try){
    printf("scale_factor_D_Ham: allocation failed for a_D_try or dot_a_D_try.\n");
    exit(1);
  };

  scale_factor_D_Ham_one_try(/* INPUTS: */
                             &a_D_integrator_params,
                             t_ODE, /* initial time */
                             h_ODE, /* initial step size */
                             a_D_0,
                             t_background_in,
                             n_t_background_in,
                             1, /* recalculate t_neg_sqrt */
                             t_neg_sqrt_initial,
                             /* OUTPUTS: */
                             (double *)&t_neg_sqrt_first_estimate,
                             a_D_try,
                             dot_a_D_try
                             );


  /* Try MAX_N_TRY_A_D_HAM t_neg_sqrt values between
     the halfmax t limits; use the best fit.

     TODO: Faster, more robust and more accurate: gsl_siman_solve.
  */

  /* find a_D_max */
  a_D_max = -9e9;
  for(i_t=0;
      i_t<n_t_background_in;
      /* &&t_background_in[i_t] <= t_neg_sqrt_first_estimate;  */
      i_t++){
    if(isnormal(a_D_try[i_t])){   /* isnormal mean is not NaN, \pm infinity, zero or subnormal */
      if(a_D_try[i_t] > a_D_max) a_D_max = a_D_try[i_t];
    };
  };
#ifdef DEBUG
  printf("a_D_max = %g\n",a_D_max);
#endif

  /* find lower and upper i_t limits for testing */
  for(i_t=0;
      i_t<n_t_background_in &&
        a_D_try[i_t] < halfmax_fraction * a_D_max;
      i_t++){
  };
  i_t_low = i_t;
  for(i_t= i_t_low + 1;
      i_t < n_t_background_in &&
        a_D_try[i_t] > halfmax_fraction * a_D_max;
      i_t++){
  };
  i_t_high = i_t-1;

#ifdef DEBUG
  printf("lowhighlimits: t_background_in[%d] = %g, t_background_in[%d] = %g\n",
         i_t_low, t_background_in[i_t_low],
         i_t_high, t_background_in[i_t_high]);
#endif

  /* initialise spline for estimates a_D derivatives */
  spline_a_D =
    gsl_spline_alloc(gsl_interp_cspline, (size_t)n_t_background_in);


  /* loop through each guess t_neg_sqrt value */
  dot_a_D_anomaly_sq_min = 9e9;
  t_neg_sqrt_best = t_background_in[i_t_low];

  /* if there's only one point, then it's already the best :( */
  if(i_t_low==i_t_high || MAX_N_TRY_A_D_HAM <= 1){
    /* store the best estimate for output */
    for(i_t=0; i_t<n_t_background_in; i_t++){
      a_D[i_t] = a_D_try[i_t];
      dot_a_D[i_t] = dot_a_D_try[i_t];
    };
  }else if(i_t_high > i_t_low){
    if(MAX_N_TRY_A_D_HAM>1){
       n_intervals_t_try_d = (double)(MAX_N_TRY_A_D_HAM-1); /* interval => include endpoints */
    }else{
       n_intervals_t_try_d = 9e19; /* arbitrary non-zero value */
    };
    delta_t_try = (t_background_in[i_t_high] - t_background_in[i_t_low])
      / n_intervals_t_try_d;
#ifdef DEBUG
    printf("delta_t_try = %g\n",
           delta_t_try);
#endif

    for(i_t_neg_sqrt=0;
        i_t_neg_sqrt < MAX_N_TRY_A_D_HAM; /* max number of estimates to try */
        i_t_neg_sqrt++){
      t_neg_sqrt_list[i_t_neg_sqrt] = t_background_in[i_t_low] +
        (double)i_t_neg_sqrt * delta_t_try;

#ifdef DEBUG
      printf("Will try t_neg_sqrt_list[%d] = %g\n",
             i_t_neg_sqrt, t_neg_sqrt_list[i_t_neg_sqrt]);
#endif

      scale_factor_D_Ham_one_try(/* INPUTS: */
                                 &a_D_integrator_params,
                                 t_ODE, /* initial time */
                                 h_ODE, /* initial step size */
                                 a_D_0,
                                 t_background_in,
                                 n_t_background_in,
                                 0, /* don't modify t_neg_sqrt */
                                 t_neg_sqrt_list[i_t_neg_sqrt],
                                 /* OUTPUTS: */
                                 (double *)&t_neg_sqrt_first_estimate, /* not the first estimate despite the name */
                                 a_D_try,
                                 dot_a_D_try
                                 );

#ifdef DEBUG
      printf("Will (re-)initialise the a_D_spline:\n");
#endif

      /* initialise a_D spline */


      gsl_spline_init( spline_a_D,
                       t_background_in,
                       a_D_try,
                       (size_t)n_t_background_in );
      dot_a_D_anomaly_sq[i_t_neg_sqrt] = 0.0;

#ifdef DEBUG
      printf("Will check if this is a better integration than before.");
#endif

      /* Given the present t_neg_sqrt estimate and the a_D_try
         table of values for all t_background_in values,
         cumulate the square differences between

         (i) the estimate of dot_a_D implied by the ODE itself based
         on the same smoothed a_D_try, versus

         (ii) the smoothed estimate
         of dot_a_D implied by the presently estimated a_D_try.

         Possibly: cumulate a penalty for positive second derivatives.
      */
      for(i_t=i_t_low; i_t <= i_t_high; i_t++){
        /*       (i) the estimate of dot_a_D implied by the ODE itself based
                 on the same smoothed a_D_try */
        scale_factor_D_Ham_ODE_func(t_background_in[i_t],
                                    (double *)(&a_D_try[i_t]),
                                    (double *)(&dot_a_D_Ham_try),
                                    (void *)(&a_D_integrator_params));
        /* difference; squared difference */
        dot_a_D_spline_try =  gsl_spline_eval_deriv(spline_a_D,
                                                    t_background_in[i_t],
                                                    accel_a_D);
        delta_anomaly = dot_a_D_spline_try - dot_a_D_Ham_try;
        dot_a_D_anomaly_sq[i_t_neg_sqrt] += delta_anomaly*delta_anomaly;

        /* penalty for positive second derivatives */
        /*
        if(i_t > i_t_low){
          if(dot_a_D_spline_try > dot_a_D_spline_try_old){
            dot_a_D_anomaly_sq[i_t_neg_sqrt] += dot_a_D_spline_try*dot_a_D_spline_try;
          };
        };
        */

#ifdef DEBUG
        if(i_t_neg_sqrt < 5){
          printf("dot_a_D_spline_try = %g, dot_a_D_Ham_try = %g, dot_a_D_try[%d] = %g\n",
                 dot_a_D_spline_try,
                 dot_a_D_Ham_try,
                 i_t, dot_a_D_try[i_t]);
        };
#endif

        /* dot_a_D_spline_try_old = dot_a_D_spline_try;  */
      }; /*    for(i_t=i_t_low; i_t <= i_t_high; i_t++) */

#ifdef DEBUG
      printf("dot_a_D_anomaly_sq[%d] = %g\n",
             i_t_neg_sqrt, dot_a_D_anomaly_sq[i_t_neg_sqrt]);
#endif

      /* store this as the best solution if it's the first time or if
         the quality parameter is better */
      if(0==i_t_neg_sqrt ||
         dot_a_D_anomaly_sq[i_t_neg_sqrt] < dot_a_D_anomaly_sq_min){
        t_neg_sqrt_best = t_neg_sqrt_list[i_t_neg_sqrt];
        dot_a_D_anomaly_sq_min = dot_a_D_anomaly_sq[i_t_neg_sqrt];

        printf("better: t_neg_sqrt_best = %g, dot_a_D_anomaly_sq_min = %g\n",
               t_neg_sqrt_best, dot_a_D_anomaly_sq_min);

        /* store the best estimate for output */
        for(i_t=0; i_t<n_t_background_in; i_t++){
          a_D[i_t] = a_D_try[i_t];
          dot_a_D[i_t] = dot_a_D_try[i_t];
        };
      }; /*       if(0==i_t_neg_sqrt ||
                  dot_a_D_anomaly_sq[i_t_neg_sqrt] < dot_a_D_anomaly_sq_min) */
    }; /*     for(i_t_neg_sqrt=0; ... ) */

  }; /* if(i_t_low==i_t_high || MAX_N_TRY_A_D_HAM <= 1) */


  /* solve the ODE - done above! */

  /* mark post-collapse times - which should generally have physically
     ridiculous values */


  for(i_t=0; i_t<n_t_background_in; i_t++){
    if(0 == i_t){
      unphysical[i_t] = 0;
    }else{
      unphysical[i_t] = unphysical[i_t-1];
    };
    if( (fabs(a_D[i_t]) >
         MAX_DEL_A_D_ACCEPTABLE *
         background_cosm_params.inhomog_a_scale_factor_now) ||
        (0 < i_t &&
         fabs(dot_a_D[i_t-1] *
              (t_background_in[i_t] -
               t_background_in[i_t-1])) >
         MAX_DEL_A_D_ACCEPTABLE *
         background_cosm_params.inhomog_a_scale_factor_now) ){
      unphysical[i_t] = 1;
    };
  };


  gsl_spline_free(spline_Q_D);
  gsl_interp_accel_free (accel_Q_D);
  gsl_spline_free(spline_R_D);
  gsl_interp_accel_free (accel_R_D);

  gsl_spline_free(spline_a_D);
  gsl_interp_accel_free (accel_a_D);

  free(rza_Q_D);
  free(rza_Q_D_minus_twoLambda_FLRW);
  free(rza_R_D);

  free(a_D_try);
  free(dot_a_D_try);
  return 0;
}
