/*
   test_scale_factor_D - based on (9) of Buchert et al RZA2 arXiv:1303.6193v2

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file test_scale_factor_D.c
 *
 * Allows for an independent test of test_scale_factor_D.c. Allows for
 * choosing planar or spherical cases and setting \a want_verbose
 * parameters.
 *
 * The calculation happens according to the Raychaudhuri version by
 * default. To use the Hamilton integration, define
 * \b INHOM_ENABLE_HAMILTON_INTEGRATOR; it can also be defined in
 * inhomog.h.
 *
 * Checks for both EdS and flat FLRW models.
 */

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif

#ifdef _OPENMP
#include <omp.h>
#endif

#include "lib/inhomog.h"

#define DEBUG 1

/* #undef DEBUG */

/* int test_scale_factor_D( */ /* INPUTS: */
int main(void)
{
  struct rza_integrand_params_s rza_integrand_params;
  struct background_cosm_params_s background_cosm_params;

#define N_PLOT_T 50
#define N_PLOT_R 2
#define N_COSM 2
  double R_domain_lower_BKS00;
  double R_domain_upper_BKS00;
  double R_domain_mod[N_PLOT_R];

  double t_i[N_COSM]; /* = 0.004574998; */ /* for z=200, H_0=50, EdS */
  double t_0[N_COSM]; /* = 13.04       */  /* for z=200, H_0=50, EdS */
  double t_background[N_COSM][N_PLOT_T];
  double a_D[N_COSM][N_PLOT_R][N_PLOT_T];
  double dot_a_D[N_COSM][N_PLOT_R][N_PLOT_T];
  int    unphysical[N_COSM][N_PLOT_R][N_PLOT_T];
  double delta_t;
  double delta_ln_R;
  double a_FLRW, a0,a1,adot0,adot1; /* local use only */
  int i_EdS;

  const gsl_rng_type * T_gsl;
  gsl_rng * r_gsl;
  static unsigned long int local_gsl_seed=0;

  long   n_calls_invariants = 300000;
  double n_sigma[3] =
  /* {2.0, 2.0, 2.0};  */
      {1.0, 1.0, 1.0};
  /* {-1.0, 0.0, 0.0}; */
  /*    {-1.0, 0.3333333333333, -0.037037037};  */

  /* int    I_invariant; */

  int i_t, i_R; /* counters in time and length scale */

  int want_planar = 0; /* cf RZA2 V.A */
  int want_spherical = 0; /* cf RZA2 V.B.3 */

  int i_thread;
  int want_verbose=0;

  double a_D_test_rel_diff[N_PLOT_R] = {0.03, 0.008};
  double dot_a_D_test_rel_diff[N_PLOT_R] = {0.05, 0.03};
  int pass=0;

  gsl_rng_env_setup();
  T_gsl = gsl_rng_default;
  gsl_rng_default_seed += 15287 + local_gsl_seed;
  local_gsl_seed = gsl_rng_default_seed;
  r_gsl = gsl_rng_alloc (T_gsl); /* this gets reallocated each time the
                                    function gets called */

  rza_integrand_params.w_type = 1; /* test standard spherical domains */

  rza_integrand_params.delta_R = DELTA_R_DEFAULT; /* used   if(2==rza_integrand_params.w_type) */

  rza_integrand_params.pow_spec_type = 'B'; /* BBKS (BKS version) default */

  rza_integrand_params.precalculated_invariants.enabled = 0;
  rza_integrand_params.sigma_sq_inv_triple.I_known = 0;
  rza_integrand_params.sigma_sq_inv_triple.II_known = 0;
  rza_integrand_params.sigma_sq_inv_triple.III_known = 0;

  printf("\ntest_scale_factor_D:\n");

  background_cosm_params.flatFLRW = 1;

  background_cosm_params.inhomog_a_scale_factor_initial = 1.0/201.0;
  background_cosm_params.inhomog_a_d_scale_factor_initial = 1.0/201.0;
  background_cosm_params.inhomog_a_scale_factor_now = 1.0;


  for(i_EdS=0; i_EdS<2; i_EdS++){
    background_cosm_params.EdS = i_EdS;
    printf("background_cosm_params.EdS  = %d\n",
           background_cosm_params.EdS);

    background_cosm_params.recalculate_t_0 = 1; /* initially recalculate for this test */
    if(1 == background_cosm_params.EdS){
      background_cosm_params.H_0 = 50.0;
      background_cosm_params.Omm_0 = 1.0;
      background_cosm_params.OmLam_0 = 0.0;
      t_i[i_EdS] = t_EdS(&background_cosm_params,
                         background_cosm_params.inhomog_a_scale_factor_initial,
                         want_verbose);
    }else if(1 == background_cosm_params.flatFLRW){
      /*    background_cosm_params.H_0 = 70.0;
            background_cosm_params.OmLam_0 = 0.70; */
      background_cosm_params.H_0 = 50.0;
      background_cosm_params.OmLam_0 = 0.001;
      background_cosm_params.Omm_0 = 1.0 - background_cosm_params.OmLam_0;
      t_i[i_EdS] = t_flatFLRW(&background_cosm_params,
                              background_cosm_params.inhomog_a_scale_factor_initial,
                              want_verbose);
    }else{
      printf("No other options for background_cosm_params so far in program.\n");
      exit(1);
    };

    background_cosm_params.recalculate_t_0 = 1;


  /* cf BKS00 Table 1, Fig 1 : final FLRW comoving units  16 Mpc, 100 Mpc */
  /*
  R_domain_lower_BKS00 = 0.08;
  R_domain_upper_BKS00 = 0.5;
  */

  /* RZA2  Fig 2 :
     physical radii approx 0.125 Mpc, 0.5 Mpc;
     final FLRW comoving diameters (2R) approx 50 Mpc, 200 Mpc */
    /* initial physical units =>  5 Mpc comoving */
    R_domain_lower_BKS00 =
      0.5 * 50.0
      *background_cosm_params.inhomog_a_scale_factor_initial
      / background_cosm_params.inhomog_a_scale_factor_now;
    /* initial physical units => 25 Mpc comoving */
    R_domain_upper_BKS00 =
      0.5 * 200.0
      *background_cosm_params.inhomog_a_scale_factor_initial
      / background_cosm_params.inhomog_a_scale_factor_now;

    t_0[i_EdS] = background_cosm_params.t_0;
    delta_t = (t_0[i_EdS]-t_i[i_EdS])/((double)(N_PLOT_T-1));
    delta_ln_R = (log(R_domain_upper_BKS00) - log(R_domain_lower_BKS00))/(double)(N_PLOT_R-1);


    for(i_t=0; i_t<N_PLOT_T; i_t++){
      t_background[i_EdS][i_t] = t_i[i_EdS] + (double)i_t * delta_t;
    };

    /* allow nested openmp if possible - so that the invariants can be calculated in parallel */
    /* #ifdef _OPENMP
       omp_set_nested(1);
       #endif
    */

#pragma omp parallel                            \
  default(shared)                               \
  private(i_R,i_thread)                         \
  firstprivate(rza_integrand_params)
    {
#pragma omp for schedule(dynamic)
      for(i_R=0; i_R<N_PLOT_R; i_R++){
        switch(rza_integrand_params.w_type)
          {
          case 1:
          default:
            R_domain_mod[i_R] = exp( log(R_domain_lower_BKS00) + ((double)i_R)* delta_ln_R );
            rza_integrand_params.R_domain = R_domain_mod[i_R];
            break;

          case 2:
            R_domain_mod[i_R] = exp( log(R_domain_lower_BKS00) + ((double)i_R)* delta_ln_R );
            rza_integrand_params.R_domain_1 = R_domain_mod[i_R] -
              0.5*rza_integrand_params.delta_R;
            rza_integrand_params.R_domain_2 = R_domain_mod[i_R] +
              0.5*rza_integrand_params.delta_R;
            break;
          };

#ifdef _OPENMP
        i_thread= omp_get_thread_num();
#else
        i_thread=0;
#endif
        (void)i_thread; /* do something with i_thread */

        /* test a_D itself */
        scale_factor_D(&rza_integrand_params,
                       background_cosm_params,
                       (double *)&(t_background[i_EdS][0]), (int)N_PLOT_T,
                       n_sigma,
                       n_calls_invariants,
                       want_planar, /* cf RZA2 V.A */
                       want_spherical, /* cf RZA2 V.B.3 */
                       want_verbose,
                       (double*)&(a_D[i_EdS][i_R][0]),
                       (double*)&(dot_a_D[i_EdS][i_R][0]),
                       (int*)&(unphysical[i_EdS][i_R][0])
                       );
      };  /*    for(i_R=0; i_R<N_PLOT_R; i_R++) */
    }; /* #pragma omp parallel                            */



    for(i_R=0; i_R<N_PLOT_R; i_R++){
      for(i_t=0; i_t<N_PLOT_T; i_t++){
        if(0==i_EdS){
          a_FLRW = a_flatFLRW(&background_cosm_params,
                              t_background[i_EdS][i_t],
                              want_verbose);
        }else{
          a_FLRW = a_EdS(&background_cosm_params,
                         t_background[i_EdS][i_t],
                         want_verbose);
        };

        printf("test_scale_factor_d r t a_D a_D/a_FLRW dot_a_D H_D: %g %g  %g %g  %g %g\n",
               R_domain_mod[i_R],
               t_background[i_EdS][i_t],
               a_D[i_EdS][i_R][i_t],
               a_D[i_EdS][i_R][i_t]/ a_FLRW,
               dot_a_D[i_EdS][i_R][i_t],
               dot_a_D[i_EdS][i_R][i_t] /
               a_D[i_EdS][i_R][i_t] /COSM_H_0_INV_GYR
             );
      };
    };
  };  /*  for(i_EdS=0; i_EdS<2; i_EdS++) */


  for(i_R=0; i_R<N_PLOT_R; i_R++){
    a0 = a_D[0][i_R][N_PLOT_T-1];
    a1 = a_D[1][i_R][N_PLOT_T-1];
    adot0 = dot_a_D[0][i_R][N_PLOT_T-1];
    adot1 = dot_a_D[1][i_R][N_PLOT_T-1];
    printf("test_scale_factor:\n");
    if( !isfinite((a0-a1)*0.5/(a0+a1)) ||
        fabs((a0-a1)*0.5/(a0+a1)) > a_D_test_rel_diff[i_R] ){
      pass += 8*i_R + 1;
      printf(" WARNING.\n");
    }
    if(  !isfinite((adot0-adot1)*0.5/(adot0+adot1)) ||
         fabs((adot0-adot1)*0.5/(adot0+adot1)) > dot_a_D_test_rel_diff[i_R] ){
      pass += 8*i_R + 2;
      printf(" WARNING.\n");
    };
    printf(" final a_D, dot_a_D EdS vs nearly-EdS at R = %g relative diff = %g %g\n",
           R_domain_mod[i_R],
           fabs((a0-a1)*0.5/(a0+a1)),
           fabs((adot0-adot1)*0.5/(adot0+adot1))
           );
    printf(" a_D = %g %g",a0, a1);
    printf(" dot_a_D = %g %g\n",adot0, adot1);
  };

  gsl_rng_free(r_gsl);

  printf("test_scale_factor_D: pass = %d\n",
         pass);
  return pass;
}
