/*
   Omega_D_precalc scale - RZA2 arXiv:1303.6193v2 - near clone of test_Omega_D

   Copyright (C) 2016-2017 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <sys/types.h>
#include <inttypes.h> /* for PRI[du]64 */
#include "config.h"
#include <math.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_statistics.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif


/*! PRECALC_PRINT_ALL should only be turned on for small simulations
    or testing purposes; it is not intended for big simulations:
*/

/* #define PRECALC_PRINT_ALL 1 */

#ifdef PRECALC_PRINT_ALL

/*! If OMEGA_GLOBAL_NORMALISE is turned on, then the Omegas are
  normalised to the global H_D (as in Roukema+2013 JCAP 10 043,
  arXiv:1303.4444) instead of the local H_D. This avoids getting
  meaningless Omega's near turnaround when the local H_D approaches
  zero.  COMMENT: This is only relevant if PRECALC_PRINT_ALL is
  enabled.
*/
#define OMEGA_GLOBAL_NORMALISE 1

/*! If OMEGA_R_D_PLANESYM and PRECALC_PRINT_ALL are both defined,
  then Omega_D_precalc will print out a modified curvature Omega
  that is useful for checking against the plane symmetric subcase
  situation. See Roukema & Ostrowski 2019 (arXiv:190m.xxxxx) for
  details.
 */
#define OMEGA_R_D_PLANESYM 1

#endif


#include "lib/inhomog.h"
#include "lib/alloc_big_array.h"

/* Calculate RZA scale factor evolution and evolution of
   effective Omegas, given a set of initial values of the
   extrinsic curvature invariants calculated on a set of
   subdomains whose disjoint union is the total domain
   (arXiv:1303.6193).
 */
void Omega_D_precalc(/* INPUTS */
                      int64_t *scal_av_n_gridsize1,
                      int64_t *scal_av_n_gridsize2,
                      int64_t *scal_av_n_gridsize3,
                      double **I_inv,
                      double **II_inv,
                      double **III_inv,
                      /* aD_norm: proportional to initial volumes^(1/3) */
                      double **aD_norm,
                      int64_t *n_t_inhomog,
                      double *a_FLRW_init,
                      double *H1bg,
                      /* OUTPUTS */
                      double **t_RZA,
                      double **a_D_RZA
                      ){

  struct rza_integrand_params_s rza_integrand_params;
  struct background_cosm_params_s background_cosm_params;

  int want_verbose = 0;
  int want_planar = 0;
  int want_spherical = 0;

  /* #define N_PLOT_T 32 */
  /* #define N_PLOT_SUBDOM 512 */
  /* #define N_PLOT_SUBDOM 4096 */
  /* #define N_PLOT_SUBDOM 64 */

  uint64_t n_subdom;
  uint64_t n_t_inhomog_u;

  /* define initial and final N_COSM values:
     1 1 gives the EdS case ("1" = true EdS)
     0 0 gives LCDM (Planck 2015 normalised)
     0 1 gives both (outputs would have to be analysed separately)
  */
#if defined INHOM_LCDM
 #define N_COSM_A 0
 #define N_COSM_B 0
#elif defined INHOM_EDS
 #define N_COSM_A 1
 #define N_COSM_B 1
#elif defined INHOM_LCDM_EDS /* both LCDM and EdS */
 #define N_COSM_A 0
 #define N_COSM_B 1
#else /* default is EdS */
 #define N_COSM_A 1
 #define N_COSM_B 1
#endif
  /* Although N_COSM is small, it is used in allocation
     routines, which require 64-bit integers for sizes */
#define N_COSM ((uint64_t)2)

#define DELTA_COLLAPSE (18.0*M_PI*M_PI)
  double Omm_z_minus_1; /* Omega_m(z) -1 = x, in (6) of Bryan & Norman (1998) */

  double t_i[N_COSM];
  double t_0[N_COSM];
  double **t_background; /* [N_COSM][N_PLOT_T]; */
  double **a_background; /* [N_COSM][N_PLOT_T]; */
  double ***a_D; /* [N_COSM][N_PLOT_SUBDOM][N_PLOT_T] */
  double *a_D_sortable;
  double ***dot_a_D;
  int *i_t_collapse;
  int *collapsed;
  double ***rza_Q_D;
  double ***rza_R_D;
  double *rza_Q_D_sortable;
  double *rza_R_D_sortable;
#ifndef HAVE_GSL_SORT2
  uint64_t *i_QR_index, i_subdom_sorted; /* sorting indices for old GSL */
#endif
  double *a_D_3_sortable_Q, *a_D_3_sortable_R;
  double a_D_3_total; /* for volume-weighted means */
  double a_D_3_half, a_D_3_cumul;  /* for volume-weighted medians */

  int ***unphysical;
  double ***H_D;
  double ***Omm_D;
  double *Omm_D_1model_1time;
  double ***OmQ_D;
  double ***OmLam_D;
  double ***OmR_D;
  double delta_t;
  double a_FLRW_expected;
  double a_dot_FLRW_expected;

  double **a_D_global; /* [N_COSM][N_PLOT_T]; */
  double **a_D_median; /* [N_COSM][N_PLOT_T]; */
  double **a_D_linear; /* [N_COSM][N_PLOT_T]; */
  double a_D_linear_per_line = 0.0;
  double a_D_linear_weight = 0.0;
  int    i_per_line, i_lines;
  double **dot_a_D_global; /* [N_COSM][N_PLOT_T]; */
  double **H_D_global;
  double **OmQ_D_global; /* [N_COSM][N_PLOT_T]; */
  double **OmR_D_global; /* [N_COSM][N_PLOT_T]; */
  double **OmQ_D_median; /* [N_COSM][N_PLOT_T]; */
  double **OmR_D_median; /* [N_COSM][N_PLOT_T]; */
#ifdef OMEGA_GLOBAL_NORMALISE
  double HDlocal_sq_over_HDglobal_sq; /* (H_D/H_D_global)^2 */
  double inv_min6Heffsq; /* -1/(6 H_eff^2) */
# ifdef OMEGA_R_D_PLANESYM
  double H_FLRW_expected;
  double OmR_D_planesym_check;
  /* term involving growth rate derivative ratio
     xiddot_xidot_term = -(2/H) * \ddot{\xi}/\dot{\xi} */
  double xiddot_xidot_term;
# endif
#endif

  double **a_D_voids; /* [N_COSM][N_PLOT_T]; */ /* global a_D for expanding domains */
  long int i_voids;
  long int i_uncollapsed;

  /* Check power spectrum normalisation:
     the L_D scale should match L_D^3 = (4\pi/8) (8 Mpc/h0eff)^3
     for the easiest cross-comparison with \sigma_8 , i.e.
     L_D = 12.9 Mpc/h0eff is about right. However, a cube is not
     a sphere, so this comparison is not perfect.
 */
  double sigma_I, sigma_Omm_D;
  double mean_I, mean_Omm_D;

  const gsl_rng_type * T_gsl;
  gsl_rng * r_gsl;
  static unsigned long int local_gsl_seed=0;

  long   n_calls_invariants = 100; /* dummy */
  double n_sigma[3] =
      {-1.0, -1.0, -1.0};

  /*  double *I_inv;
  double *II_inv;
  double *III_inv;
  */

#ifndef errno
  extern int errno;
#endif

  unsigned int i_t; /* time iterator */
  uint64_t i_subdom; /* iterator over subdomains, each with an I, II and III value */
  int i_EdS;

  int alloc_failed = 0; /* Quit nicely if memory allocation not
                           possible; 0 mains memory allocated OK. */

  gsl_rng_env_setup();
  T_gsl = gsl_rng_default;
  gsl_rng_default_seed += 2467 + local_gsl_seed;
  local_gsl_seed = gsl_rng_default_seed;
  r_gsl = gsl_rng_alloc (T_gsl); /* this gets reallocated each time the
                                    function gets called */

  n_t_inhomog_u = (uint64_t)(*n_t_inhomog);

#ifdef __GNUC__
  /* check allocation */
  printf("malloc_usable_size(*I_inv) = %zu malloc_usable_size(*II_inv) = %zu  (*scal_av_n_gridsize1) = %" PRId64 "\n",
         malloc_usable_size(*I_inv), malloc_usable_size(*II_inv), (*scal_av_n_gridsize1));
#endif

  printf("Omega_D_precalc: n_t_inhomog = %p, *n_t_inhomog = %" PRId64 ", n_t_inhomog_u = %" PRIu64 "\n",
         (void*)n_t_inhomog, *n_t_inhomog, n_t_inhomog_u);

  n_subdom = (uint64_t)(*scal_av_n_gridsize1)
    * (uint64_t)(*scal_av_n_gridsize2)
    * (uint64_t)(*scal_av_n_gridsize3);


  alloc_failed += alloc_big_array_3D_d(N_COSM,n_subdom,n_t_inhomog_u,
                       &a_D, 1);
  alloc_failed += alloc_big_array_1D_d(n_subdom,
                       &a_D_sortable, 1);
  alloc_failed += alloc_big_array_1D_d(n_subdom,
                       &rza_Q_D_sortable, 1);
  alloc_failed += alloc_big_array_1D_d(n_subdom,
                       &rza_R_D_sortable, 1);
  alloc_failed += alloc_big_array_1D_d(n_subdom,
                       &a_D_3_sortable_Q, 1);
  alloc_failed += alloc_big_array_1D_d(n_subdom,
                       &a_D_3_sortable_R, 1);
  alloc_failed += alloc_big_array_3D_d(N_COSM,n_subdom,n_t_inhomog_u,
                       &dot_a_D, 1);
  i_t_collapse = malloc((size_t)(n_subdom*sizeof(int)));
  collapsed = malloc((size_t)(n_subdom*sizeof(int)));

  alloc_failed += alloc_big_array_3D_i(N_COSM,n_subdom,n_t_inhomog_u,
                       &unphysical, 1);
  alloc_failed += alloc_big_array_3D_d(N_COSM,n_subdom,n_t_inhomog_u,
                       &rza_Q_D, 1);
  alloc_failed += alloc_big_array_3D_d(N_COSM,n_subdom,n_t_inhomog_u,
                       &rza_R_D, 1);


  alloc_failed += alloc_big_array_3D_d(N_COSM,n_subdom,n_t_inhomog_u,
                       &H_D, 1);
  alloc_failed += alloc_big_array_3D_d(N_COSM,n_subdom,n_t_inhomog_u,
                       &Omm_D, 1);
  alloc_failed += alloc_big_array_1D_d(n_subdom,
                       &Omm_D_1model_1time, 1);

  alloc_failed += alloc_big_array_3D_d(N_COSM,n_subdom,n_t_inhomog_u,
                       &OmQ_D, 1);
  alloc_failed += alloc_big_array_3D_d(N_COSM,n_subdom,n_t_inhomog_u,
                       &OmR_D, 1);
  alloc_failed += alloc_big_array_3D_d(N_COSM,n_subdom,n_t_inhomog_u,
                       &OmLam_D, 1);

  alloc_failed += alloc_big_array_2D_d(N_COSM,n_t_inhomog_u,
                       &t_background, 1);
  alloc_failed += alloc_big_array_2D_d(N_COSM,n_t_inhomog_u,
                       &a_background, 1);
  alloc_failed += alloc_big_array_2D_d(N_COSM,n_t_inhomog_u,
                       &a_D_global, 1);
  alloc_failed += alloc_big_array_2D_d(N_COSM,n_t_inhomog_u,
                       &a_D_median, 1);
  alloc_failed += alloc_big_array_2D_d(N_COSM,n_t_inhomog_u,
                       &a_D_linear, 1);
  alloc_failed += alloc_big_array_2D_d(N_COSM,n_t_inhomog_u,
                       &dot_a_D_global, 1);
  alloc_failed += alloc_big_array_2D_d(N_COSM,n_t_inhomog_u,
                       &H_D_global, 1);
  alloc_failed += alloc_big_array_2D_d(N_COSM,n_t_inhomog_u,
                       &OmQ_D_global, 1);
  alloc_failed += alloc_big_array_2D_d(N_COSM,n_t_inhomog_u,
                       &OmR_D_global, 1);
  alloc_failed += alloc_big_array_2D_d(N_COSM,n_t_inhomog_u,
                       &OmQ_D_median, 1);
  alloc_failed += alloc_big_array_2D_d(N_COSM,n_t_inhomog_u,
                       &OmR_D_median, 1);
  alloc_failed += alloc_big_array_2D_d(N_COSM,n_t_inhomog_u,
                       &a_D_voids, 1);

  if(alloc_failed){
    printf("ERROR: in %s at line %d: out of memory; alloc_failed = %d , alloc_failed/ENOMEM = %d.\n",
           __FILE__,
           __LINE__,
           alloc_failed, alloc_failed/ENOMEM);
    exit(1);
  };

  /*
  I_inv = malloc(n_subdom*sizeof(double));
  II_inv = malloc(n_subdom*sizeof(double));
  III_inv = malloc(n_subdom*sizeof(double));
  */


  rza_integrand_params.w_type = 1; /* test standard spherical domains */

  rza_integrand_params.delta_R = DELTA_R_DEFAULT; /*   if(2==rza_integrand_params.w_type) */

  /* 'B' = BBKS (BKS version); 'e' = EisHu98 short formula set ;
     'E' = EisHu98 full formula set */
  rza_integrand_params.pow_spec_type = 'E';

  rza_integrand_params.precalculated_invariants.enabled = 1;


  printf("Omega_D_precalc: number of subdomains: %" PRIu64 "\n",n_subdom);


  background_cosm_params.flatFLRW = 1;

  background_cosm_params.inhomog_a_scale_factor_initial = *a_FLRW_init;
  background_cosm_params.inhomog_a_d_scale_factor_initial = *a_FLRW_init;
  background_cosm_params.inhomog_a_scale_factor_now = 1.0;


  for(i_EdS=N_COSM_A; i_EdS<(int)N_COSM_B+1; i_EdS++){
    background_cosm_params.EdS = i_EdS;
    printf("background_cosm_params.EdS  = %d\n",
           background_cosm_params.EdS);

    background_cosm_params.recalculate_t_0 = 1; /* initially recalculate for this test */
    background_cosm_params.Theta2p7 = 2.726/2.7; /* e.g. arXiv:0911.1955 */
    if(1 == background_cosm_params.EdS){
      /* TODO: integrate these values with those in other routines. */
      /* background_cosm_params.H_0 = 37.7; */
      background_cosm_params.H_0 = *H1bg;
      background_cosm_params.Omm_0 = 1.0;
      background_cosm_params.OmLam_0 = 0.0;
      background_cosm_params.Ombary_0 = 0.022
        /exp(2.0*log(background_cosm_params.H_0 / 100));
      /*
         Planck 2015: arXiv:1502.01589, Table 4, final column,
         adjusted to for CMB-matched EdS; see
         RMOB17 arXiv:1608.06004)
      */
      background_cosm_params.sigma8 = 1.0395;

      t_i[i_EdS] = t_EdS(&background_cosm_params,
                         background_cosm_params.inhomog_a_scale_factor_initial,
                         want_verbose);
    }else if(1 == background_cosm_params.flatFLRW){
      /*
         WMAP:
         background_cosm_params.H_0 = 70.0;
         background_cosm_params.OmLam_0 = 0.73;
      */
      /*
         Planck 2015: arXiv:1502.01589, Table 4, final column
      */
      background_cosm_params.H_0 = 67.74;
      background_cosm_params.OmLam_0 = 0.6911;
      background_cosm_params.Ombary_0 = 0.044;
      background_cosm_params.sigma8 = 0.8159;
      /*
        background_cosm_params.H_0 = 50.0;
        background_cosm_params.OmLam_0 = 0.001;
      */
      background_cosm_params.Omm_0 = 1.0 - background_cosm_params.OmLam_0;
      t_i[i_EdS] = t_flatFLRW(&background_cosm_params,
                              background_cosm_params.inhomog_a_scale_factor_initial,
                              want_verbose);
    }else{
      printf("No other options for background_cosm_params so far in program.\n");
      exit(1);
    };

    background_cosm_params.recalculate_t_0 = 1;


    printf("\nOmega_D_precalc:\n");

    t_0[i_EdS] = background_cosm_params.t_0;
    delta_t = (t_0[i_EdS]-t_i[i_EdS])/((double)(n_t_inhomog_u-1));

    for(i_t=0; i_t<n_t_inhomog_u; i_t++){
      t_background[i_EdS][i_t] = t_i[i_EdS] + (double)i_t * delta_t;
    };

    /* allow nested openmp if possible - so that the invariants can be calculated in parallel */
    /* #ifdef _OPENMP
       omp_set_nested(1);
       #endif
    */


    mean_I = gsl_stats_mean( *I_inv, (size_t)1, (size_t)n_subdom);
    sigma_I = gsl_stats_sd( *I_inv, (size_t)1, (size_t)n_subdom);


#pragma omp parallel                                                    \
  default(shared)                                                       \
  private(i_subdom)                                                     \
  firstprivate(rza_integrand_params,                                    \
               background_cosm_params)
    {
#pragma omp for schedule(dynamic)
      for(i_subdom=0; i_subdom<n_subdom; i_subdom++){

        printf("I,II,III = %g %g %g\n",
               (*I_inv)[i_subdom], (*II_inv)[i_subdom], (*III_inv)[i_subdom]);

        rza_integrand_params.precalculated_invariants.inv_I = (*I_inv)[i_subdom];
        rza_integrand_params.precalculated_invariants.inv_II = (*II_inv)[i_subdom];
        rza_integrand_params.precalculated_invariants.inv_III = (*III_inv)[i_subdom];

        Omega_D(&rza_integrand_params,
                background_cosm_params,
                (double*)&(t_background[i_EdS][0]), (int)(*n_t_inhomog),
                n_sigma,
                n_calls_invariants,
                want_planar, /* cf RZA2 V.A */
                want_spherical,
                want_verbose,
                (double*)&(rza_Q_D[i_EdS][i_subdom][0]),
                (double*)&(rza_R_D[i_EdS][i_subdom][0]),
                (double*)&(a_D[i_EdS][i_subdom][0]),
                (double*)&(dot_a_D[i_EdS][i_subdom][0]),
                (int*)&(unphysical[i_EdS][i_subdom][0]),
                (double*)&(H_D[i_EdS][i_subdom][0]),
                (double*)&(Omm_D[i_EdS][i_subdom][0]),
                (double*)&(OmQ_D[i_EdS][i_subdom][0]),
                (double*)&(OmLam_D[i_EdS][i_subdom][0]),
                (double*)&(OmR_D[i_EdS][i_subdom][0])
                );

        find_collapse_time((int)(*n_t_inhomog),
                           (double*)&(a_D[i_EdS][i_subdom][0]),
                           (double*)&(dot_a_D[i_EdS][i_subdom][0]),
                           (int*)&(i_t_collapse[i_subdom]),
                           (int*)&(collapsed[i_subdom]));
      };  /*    for(i_subdom=0; i_subdom<n_subdom; i_subdom++) */

    }; /* #pragma omp parallel                            */

    for(i_t=0; i_t<n_t_inhomog_u; i_t++){
      a_D_global[i_EdS][i_t] = 0.0;
      a_D_voids[i_EdS][i_t] = 0.0;

      OmQ_D_global[i_EdS][i_t] = 0.0;
      OmR_D_global[i_EdS][i_t] = 0.0;

      i_voids = 0;
      i_uncollapsed = 0;
      a_D_3_total = 0.0;

      a_D_linear[i_EdS][i_t] = 0.0;
      i_per_line = 0;
      i_lines = 0;

      if(1==i_EdS){
        a_FLRW_expected =
          a_EdS(&background_cosm_params,
                     t_background[i_EdS][i_t],
                     want_verbose);
      }else{
        a_FLRW_expected =
          a_flatFLRW(&background_cosm_params,
                     t_background[i_EdS][i_t],
                     want_verbose);
      };
      /* TODO: remove redundant recalculations of a_background
         below */
      a_background[i_EdS][i_t] = a_FLRW_expected;

      for(i_subdom=0; i_subdom<n_subdom; i_subdom++){

        /* If a line (e.g. of constant y, z) is finished, then
           cumulate the mean linear scale factor and start a new
           line.

           This option is distracting and should probably be
           removed.
        */
        if(i_per_line+1 == *scal_av_n_gridsize1){
          a_D_linear[i_EdS][i_t] +=
            a_D_linear_per_line/fmax(1.0,a_D_linear_weight);
          i_lines ++;
          i_per_line = 0;
          a_D_linear_per_line = 0.0;
          a_D_linear_weight = 0.0;
        }else{
          i_per_line++;
        };


        if( !collapsed[i_subdom] ||
             ( collapsed[i_subdom] &&
               i_t_collapse[i_subdom] >= 0 &&
               i_t <= (unsigned)i_t_collapse[i_subdom] )
            ){

          /* Initial volume weighting: correct for the case of unequal
             initial volumes of the domains D */
          a_D[i_EdS][i_subdom][i_t] *= (*aD_norm)[i_subdom];

          /* volume element a_D^3 weightings */
          a_D_3_sortable_Q[i_subdom] = pow(a_D[i_EdS][i_subdom][i_t], 3.0);
          a_D_3_sortable_R[i_subdom] = a_D_3_sortable_Q[i_subdom];
          a_D_3_total += a_D_3_sortable_Q[i_subdom];

          /* cumulate uncollapsed volumes */
          a_D_global[i_EdS][i_t] += a_D_3_sortable_Q[i_subdom];
          a_D_linear_per_line += a_D_3_sortable_Q[i_subdom];
          a_D_linear_weight += pow(a_D[i_EdS][i_subdom][i_t], 2.0);

          a_D_sortable[i_subdom] = a_D[i_EdS][i_subdom][i_t];

          /* cumulative Ricci scalar curvatures (not Omegas) */
          OmQ_D_global[i_EdS][i_t] += rza_Q_D[i_EdS][i_subdom][i_t]
            * a_D_3_sortable_Q[i_subdom];
          rza_Q_D_sortable[i_subdom] = rza_Q_D[i_EdS][i_subdom][i_t];

          OmR_D_global[i_EdS][i_t] += rza_R_D[i_EdS][i_subdom][i_t]
            * a_D_3_sortable_R[i_subdom];
          rza_R_D_sortable[i_subdom] = rza_R_D[i_EdS][i_subdom][i_t];

          i_uncollapsed++;

          if(dot_a_D[i_EdS][i_subdom][i_t] > 0.0
             /* I_inv[i_subdom] > 0.0 */
             ){
            a_D_voids[i_EdS][i_t] += pow(a_D[i_EdS][i_subdom][i_t], 3.0);
            i_voids++;
          };
        }else /* post-collapse cases */ {
          if(1==i_EdS){
            a_D_global[i_EdS][i_t] +=
              pow( a_EdS(&background_cosm_params,
                         t_background[i_EdS][i_t_collapse[i_subdom]],
                         want_verbose), 3.0 ) /
              DELTA_COLLAPSE;
            a_D_linear_per_line +=
              pow( a_EdS(&background_cosm_params,
                         t_background[i_EdS][i_t_collapse[i_subdom]],
                         want_verbose), 3.0 ) /
              DELTA_COLLAPSE;
            a_D_linear_weight +=
              pow( a_EdS(&background_cosm_params,
                         t_background[i_EdS][i_t_collapse[i_subdom]],
                         want_verbose), 2.0 ) /
              pow( DELTA_COLLAPSE, 2.0/3.0 );

          }else{
            a_FLRW_expected =
              a_flatFLRW(&background_cosm_params,
                         t_background[i_EdS][i_t],
                         want_verbose);
            Omm_z_minus_1 = background_cosm_params.Omm_0 /
              pow(a_FLRW_expected, 3.0) /
              ( background_cosm_params.Omm_0 /
                pow(a_FLRW_expected, 3.0)
                /* + 0.0 */ /* zero FLRW curvature */
                + background_cosm_params.OmLam_0 )  - 1.0;
            /* printf("Omm_z_minus_1 = %g\n",Omm_z_minus_1); */

            a_D_global[i_EdS][i_t] +=
              pow( a_flatFLRW(&background_cosm_params,
                              t_background[i_EdS][i_t_collapse[i_subdom]],
                              want_verbose), 3.0 ) /
              /* TODO: Modularise out this implementation of (6) of
                 Bryan & Norman 1998
                 http://iopscience.iop.org/article/10.1086/305262/pdf
                 which is fit to Eke et al 1996
                 http://mnras.oxfordjournals.org/content/282/1/263.full.pdf

                 Octave visual check:

                 omm=linspace(0.1,1,50)
                 plot(omm, ((18*pi^2).+ 82.*(omm.-1) .-39.*(omm.-1).^2),'-')
                 axis([0 1 50 200])
               */
              (DELTA_COLLAPSE + 82.0*Omm_z_minus_1
               - 39.0*Omm_z_minus_1*Omm_z_minus_1);

            a_D_linear_per_line +=
              pow( a_flatFLRW(&background_cosm_params,
                              t_background[i_EdS][i_t_collapse[i_subdom]],
                              want_verbose), 3.0) /
              /* the virialisation overdensity should be around 100-200, i.e. >> 1.0 */
              fmax(1.0, (DELTA_COLLAPSE + 82.0*Omm_z_minus_1
                         - 39.0*Omm_z_minus_1*Omm_z_minus_1));
            a_D_linear_weight +=
              pow( a_flatFLRW(&background_cosm_params,
                              t_background[i_EdS][i_t_collapse[i_subdom]],
                              want_verbose), 2.0) /
              /* the virialisation overdensity should be around 100-200, i.e. >> 1.0 */
              pow(fmax(1.0, (DELTA_COLLAPSE + 82.0*Omm_z_minus_1
                             - 39.0*Omm_z_minus_1*Omm_z_minus_1)), 2.0/3.0);

          };

          a_D_3_sortable_Q[i_subdom] = 0.0;
          a_D_3_sortable_R[i_subdom] = 0.0;
          rza_Q_D_sortable[i_subdom] = -9e9;
          rza_R_D_sortable[i_subdom] = -9e9;
        }; /*  post-collapse cases */
      }; /* for(i_subdom=0; i_subdom<n_subdom; i_subdom++) */


      if(0 == i_t){
        for(i_subdom=0; i_subdom<n_subdom; i_subdom++){
          Omm_D_1model_1time[i_subdom] = Omm_D[i_EdS][i_subdom][i_t];
        };
        mean_Omm_D = gsl_stats_mean( Omm_D_1model_1time, (size_t)1, (size_t)n_subdom);
        sigma_Omm_D = gsl_stats_sd( Omm_D_1model_1time, (size_t)1, (size_t)n_subdom);
        printf("a_FLRW_init, mean_I, mean_Omm_D; EdS-grown-values: %g %g %g\n",
               *a_FLRW_init, mean_I, mean_Omm_D);
        printf("a_FLRW_init, sigma_I, sigma_Omm_D; EdS-grown-values: %g %g %g : %g %g\n",
               *a_FLRW_init, sigma_I, sigma_Omm_D,
               sigma_I/(*a_FLRW_init), sigma_Omm_D/(a_FLRW_expected));
      };
      /* -> mean volume -> a_D_global */
      if(isnormal(a_D_global[i_EdS][i_t]) &&
         a_D_global[i_EdS][i_t] > 1e-100){
        a_D_global[i_EdS][i_t] =
          pow( (a_D_global[i_EdS][i_t]/(double)n_subdom),
               1.0/3.0 );
      }else{
        a_D_global[i_EdS][i_t] = -9e99;
      };


      if(isnormal(a_D_linear[i_EdS][i_t])){
        a_D_linear[i_EdS][i_t] =
          a_D_linear[i_EdS][i_t]/(double)i_lines;
      }else{
        a_D_linear[i_EdS][i_t] = -9e99;
      };

      gsl_sort(a_D_sortable, (size_t)1, (size_t)n_subdom); /* sorts in place */
      a_D_median[i_EdS][i_t] =
        gsl_stats_median_from_sorted_data(a_D_sortable,
                                          (size_t)1, (size_t)n_subdom);

      /* Outputs for fortran calling routine. */
      if(1==i_EdS){
        (*t_RZA)[i_t] = t_background[i_EdS][i_t];
        (*a_D_RZA)[i_t] = a_D_global[i_EdS][i_t];
      };


      if(isnormal(a_D_voids[i_EdS][i_t]) &&
         a_D_voids[i_EdS][i_t] > TOL_A_D_MIN && i_voids > 0){
        a_D_voids[i_EdS][i_t] =
          pow( (a_D_voids[i_EdS][i_t]/(double)i_voids),
               1.0/3.0 );
      }else{
        a_D_voids[i_EdS][i_t] = -9e99;
      };

      if(1==i_EdS){
        a_FLRW_expected =
          a_EdS(&background_cosm_params,
                t_background[i_EdS][i_t],
                want_verbose);
        a_dot_FLRW_expected =
          a_dot_EdS(&background_cosm_params,
                    t_background[i_EdS][i_t],
                    want_verbose);
      }else{
        a_FLRW_expected =
          a_flatFLRW(&background_cosm_params,
                     t_background[i_EdS][i_t],
                     want_verbose);
        a_dot_FLRW_expected =
          a_dot_flatFLRW(&background_cosm_params,
                     t_background[i_EdS][i_t],
                     want_verbose);
      };

      if(0 == i_t){
        /* set the first time step to have the FLRW values */
        dot_a_D_global[i_EdS][i_t] = a_dot_FLRW_expected;
        H_D_global[i_EdS][i_t] = a_dot_FLRW_expected/
          a_FLRW_expected;
      }else{
        dot_a_D_global[i_EdS][i_t] =
          (a_D_global[i_EdS][i_t] -a_D_global[i_EdS][i_t-1])/
          (t_background[i_EdS][i_t]-t_background[i_EdS][i_t-1]) ;

        if(a_D_global[i_EdS][i_t] > TOL_A_D_MIN){
          H_D_global[i_EdS][i_t] = dot_a_D_global[i_EdS][i_t] / a_D_global[i_EdS][i_t];
        }else{
          H_D_global[i_EdS][i_t] = -9e9;
        };
      };

      if(a_D_3_total > 0.0){
        /* volume-weighted means */
        OmQ_D_global[i_EdS][i_t] =
          -(OmQ_D_global[i_EdS][i_t]/a_D_3_total)
          /(H_D_global[i_EdS][i_t]*H_D_global[i_EdS][i_t]) /6.0;
        OmR_D_global[i_EdS][i_t] =
          -(OmR_D_global[i_EdS][i_t]/a_D_3_total)
          /(H_D_global[i_EdS][i_t]*H_D_global[i_EdS][i_t]) /6.0;

        /* volume-weighted medians */
        a_D_3_half = 0.5* a_D_3_total;

        /* Sort a_D_3_sortable_Q according to the ascending order of
           rza_Q_D_sortable. */

#ifdef HAVE_GSL_SORT2
        gsl_sort2(rza_Q_D_sortable, (size_t)1,
                  a_D_3_sortable_Q, (size_t)1, (size_t)n_subdom);
#else
        /* Old GSL:  */
        if(NULL == (i_QR_index = malloc(n_subdom*sizeof(uint64_t)))){
          printf("Omega_D_precalc: Error: out of memory.\n");
          exit(1);
        };
        gsl_sort_index(i_QR_index, rza_Q_D_sortable,
                       (size_t)1, n_subdom);
        gsl_sort(rza_Q_D_sortable, (size_t)1, n_subdom);
        for(i_subdom = 0; i_subdom < n_subdom; i_subdom++){
          i_subdom_sorted = i_QR_index[i_subdom];
          if( !collapsed[i_subdom_sorted] ||
              ( collapsed[i_subdom_sorted] &&
                i_t_collapse[i_subdom_sorted] >= 0 &&
                i_t <= (unsigned)i_t_collapse[i_subdom_sorted] )
              ){
            a_D_3_sortable_Q[i_subdom] = pow(a_D[i_EdS][i_subdom_sorted][i_t], 3.0);
          }else{
            a_D_3_sortable_Q[i_subdom] = 0.0;
          };
        };
        gsl_sort_index(i_QR_index, rza_R_D_sortable,
                       (size_t)1, n_subdom);
        gsl_sort(rza_R_D_sortable, (size_t)1, n_subdom);
        for(i_subdom = 0; i_subdom < n_subdom; i_subdom++){
          i_subdom_sorted = i_QR_index[i_subdom];
          if( !collapsed[i_subdom_sorted] ||
              ( collapsed[i_subdom_sorted] &&
                i_t_collapse[i_subdom_sorted] >= 0 &&
                i_t <= (unsigned)i_t_collapse[i_subdom_sorted] )
              ){
            a_D_3_sortable_R[i_subdom] = pow(a_D[i_EdS][i_subdom_sorted][i_t], 3.0);
          }else{
            a_D_3_sortable_R[i_subdom] = 0.0;
          };
        };
        free(i_QR_index);
#endif

        a_D_3_cumul = 0.0;
        /* Find position of weighted median in array. */
        for(i_subdom = 0;
            i_subdom < n_subdom &&
              a_D_3_cumul < a_D_3_half;
            i_subdom++){
          a_D_3_cumul += a_D_3_sortable_Q[i_subdom];
        };
        if(i_subdom >= n_subdom) i_subdom = n_subdom-1;
        OmQ_D_median[i_EdS][i_t] =
          - rza_Q_D_sortable[i_subdom]
          /(H_D_global[i_EdS][i_t]*H_D_global[i_EdS][i_t]) /6.0;

        /* Sort a_D_3_sortable_R according to the ascending order of
           rza_R_D_sortable. */
#ifdef HAVE_GSL_SORT2
        gsl_sort2(rza_R_D_sortable, (size_t)1,
                  a_D_3_sortable_R, (size_t)1, (size_t)n_subdom);
        /* without gsl_sort2: done above */
#endif
        a_D_3_cumul = 0.0;
        /* Find position of weighted median in array. */
        for(i_subdom = 0;
            i_subdom < n_subdom &&
              a_D_3_cumul < a_D_3_half;
            i_subdom++){
          a_D_3_cumul += a_D_3_sortable_R[i_subdom];
        };
        if(i_subdom >= n_subdom) i_subdom = n_subdom-1;
        OmR_D_median[i_EdS][i_t] =
          - rza_R_D_sortable[i_subdom]
          /(H_D_global[i_EdS][i_t]*H_D_global[i_EdS][i_t]) /6.0;

      }else{
        OmQ_D_global[i_EdS][i_t] = -99.9;
        OmR_D_global[i_EdS][i_t] = -99.9;
        OmQ_D_median[i_EdS][i_t] = -99.9;
        OmR_D_median[i_EdS][i_t] = -99.9;
      };

      /* TODO: optionally turn this off for high (*n_t_inhomog). */
      printf("a_D_global: %e %e : %e %e : ",
             t_background[i_EdS][i_t],
             a_FLRW_expected,
             /* : */
             a_D_global[i_EdS][i_t],
             a_D_voids[i_EdS][i_t]
             /* : */
             );
      /* bidomain case */
      if(2==n_subdom){
        printf(" %e %e : ",
               a_D[i_EdS][0][i_t],
               a_D[i_EdS][1][i_t]);
      };

      printf("%e %e : %e %e %e %e %ld\n",
             dot_a_D_global[i_EdS][i_t],
             H_D_global[i_EdS][i_t] *KMS_PER_MPCGYR,
             /* : */
             OmQ_D_global[i_EdS][i_t],
             OmQ_D_median[i_EdS][i_t],
             OmR_D_global[i_EdS][i_t],
             OmR_D_median[i_EdS][i_t],
             /* : */
             i_uncollapsed
             );
    };

    for(i_subdom=0; i_subdom<n_subdom; i_subdom++){
      for(i_t=0;
          (i_t_collapse[i_subdom] >= 0 &&
           i_t <= (unsigned)i_t_collapse[i_subdom]);
          i_t++){

#ifdef PRECALC_PRINT_ALL

# ifdef OMEGA_GLOBAL_NORMALISE
        HDlocal_sq_over_HDglobal_sq =
          pow(H_D[i_EdS][i_subdom][i_t]/H_D_global[i_EdS][i_t], 2.0);
        inv_min6Heffsq =
          -(1.0e0/6.0e0) /pow(H_D_global[i_EdS][i_t], 2.0);

#  ifdef OMEGA_R_D_PLANESYM
        if(1==i_EdS){
          a_FLRW_expected =
            a_EdS(&background_cosm_params,
                  t_background[i_EdS][i_t],
                  want_verbose);
          a_dot_FLRW_expected =
            a_dot_EdS(&background_cosm_params,
                  t_background[i_EdS][i_t],
                  want_verbose);
        }else{
          a_FLRW_expected =
            a_flatFLRW(&background_cosm_params,
                       t_background[i_EdS][i_t],
                       want_verbose);
          a_dot_FLRW_expected =
            a_dot_flatFLRW(&background_cosm_params,
                           t_background[i_EdS][i_t],
                           want_verbose);
        };
        H_FLRW_expected = a_dot_FLRW_expected/a_FLRW_expected;

        if(1 == background_cosm_params.EdS){
          /* - (2/H_FLRW_expected) * (-1/2) H_FLRW_expected = 1 */
          xiddot_xidot_term = 1.0;
        } else if(1 == background_cosm_params.flatFLRW){
          /* growth_FLRW is for the 'q' function, but since first and
             second derivatives of \xi are used here, the normalisation
             and zeropoint both disappear;
             \ddot{\xi}/\dot{\xi} = \ddot{q}/\dot{q}
          */
          xiddot_xidot_term = -2.0e0 / H_FLRW_expected *
            ddot_growth_FLRW(&background_cosm_params,
                                 t_background[i_EdS][i_t],
                                 want_verbose) /
            dot_growth_FLRW(&background_cosm_params,
                           t_background[i_EdS][i_t],
                           want_verbose);
        }else{
          printf("Omega_D_precalc: Warning: xiddot_xidot_term: No other options for background_cosm_params so far in program.\n");
          /* This error will give wrong results, but these are not the main results,
             so at least for the moment, give a warning rather than an error.
          */
          xiddot_xidot_term = -9e9;
        };

        /* OmR_D_planesym_check := Omega_R^D [normalised using H_FLRW^2] -
           xiddot_xidot_term */
        OmR_D_planesym_check =
          rza_R_D[i_EdS][i_subdom][i_t] *
          (-1.0e0/6.0e0) / pow(H_FLRW_expected, 2.0) -
          xiddot_xidot_term;
#  endif /* OMEGA_R_D_PLANESYM */
# endif /* OMEGA_GLOBAL_NORMALISE */

        printf(/*nsig=%4.1f%4.1f%4.1f*/
#if defined(OMEGA_GLOBAL_NORMALISE) && defined(OMEGA_R_D_PLANESYM)
               " tB2 z Heff i : a_D H_D : Omm_D OmQ_D OmR_D  %g %g %g %" PRIu64 " : %g %g : %g %g %g %g\n",
#  else
               " tB2 z Heff i : a_D H_D : Omm_D OmQ_D OmR_D  %g %g %g %" PRIu64 " : %g %g : %g %g %g\n",
#  endif
               /* n_sigma[0],
                  n_sigma[1],
                  n_sigma[2],*/
               t_background[i_EdS][i_t],
               1.0/a_background[i_EdS][i_t]-1.0,
               H_D_global[i_EdS][i_t]/COSM_H_0_INV_GYR,
               i_subdom,
               a_D[i_EdS][i_subdom][i_t],
               H_D[i_EdS][i_subdom][i_t]/COSM_H_0_INV_GYR,
# ifdef OMEGA_GLOBAL_NORMALISE
               Omm_D[i_EdS][i_subdom][i_t] *HDlocal_sq_over_HDglobal_sq,
               rza_Q_D[i_EdS][i_subdom][i_t] *inv_min6Heffsq,
               /* OmLam_D[i_EdS][i_subdom][i_t], */
               rza_R_D[i_EdS][i_subdom][i_t] *inv_min6Heffsq
#  ifdef OMEGA_R_D_PLANESYM
               , OmR_D_planesym_check
#  endif
# else
               Omm_D[i_EdS][i_subdom][i_t],
               OmQ_D[i_EdS][i_subdom][i_t],
               /* OmLam_D[i_EdS][i_subdom][i_t], */
               OmR_D[i_EdS][i_subdom][i_t]
# endif /* OMEGA_GLOBAL_NORMALISE */
               );
#endif /* PRECALC_PRINT_ALL */
      }; /* for(i_t=0; i_t<(*n_t_inhomog); i_t++) */
    }; /* for(i_subdom=0; i_subdom<n_subdom; i_subdom++) */

  }; /*   for(i_EdS=0; i_EdS<N_COSM; i_EdS++) */


  free_big_array_2D_d(N_COSM, a_D_voids);
  free_big_array_2D_d(N_COSM, OmR_D_median);
  free_big_array_2D_d(N_COSM, OmQ_D_median);
  free_big_array_2D_d(N_COSM, OmR_D_global);
  free_big_array_2D_d(N_COSM, OmQ_D_global);
  free_big_array_2D_d(N_COSM, H_D_global);
  free_big_array_2D_d(N_COSM, dot_a_D_global);
  free_big_array_2D_d(N_COSM, a_D_linear);
  free_big_array_2D_d(N_COSM, a_D_median);
  free_big_array_2D_d(N_COSM, a_D_global);
  free_big_array_2D_d(N_COSM, a_background);
  free_big_array_2D_d(N_COSM, t_background);

  free_big_array_3D_d(N_COSM,n_subdom,
                      dot_a_D);
  free_big_array_1D_d(a_D_3_sortable_R);
  free_big_array_1D_d(a_D_3_sortable_Q);
  free_big_array_1D_d(rza_R_D_sortable);
  free_big_array_1D_d(rza_Q_D_sortable);
  free_big_array_1D_d(a_D_sortable);
  free_big_array_3D_d(N_COSM,n_subdom,
                      a_D);
  free(i_t_collapse);
  free(collapsed);

  free_big_array_3D_d(N_COSM,n_subdom,
                      OmLam_D);
  free_big_array_3D_d(N_COSM,n_subdom,
                      OmR_D);
  free_big_array_3D_d(N_COSM,n_subdom,
                      OmQ_D);
  free_big_array_1D_d(Omm_D_1model_1time);
  free_big_array_3D_d(N_COSM,n_subdom,
                      Omm_D);
  free_big_array_3D_d(N_COSM,n_subdom,
                      H_D);

  free_big_array_3D_d(N_COSM,n_subdom,
                      rza_R_D);
  free_big_array_3D_d(N_COSM,n_subdom,
                      rza_Q_D);
  free_big_array_3D_i(N_COSM,n_subdom,
                      unphysical);


  gsl_rng_free(r_gsl);

  return; /* fortran doesn't use a return value */
}
