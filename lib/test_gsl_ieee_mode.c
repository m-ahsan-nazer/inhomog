/* 
   test_gsl_ieee_mode

   Copyright (C) 2016 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

   See also http://www.gnu.org/licenses/gpl.html
   
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_ieee_utils.h>

/* All of the scientifically acceptable errors should be covered by:

   GSL_IEEE_MODE="double-precision, mask-underflow, mask-denormalized" \
   ./lib/test_gsl_ieee_mode

*/

/* test if the IEEE mode applies to separate functions */
int div_by_zero(void);

int div_by_zero(void){
  printf("1.0/0.0 = %20.15g\n",
         1.0/0.0);
  return 0;
}



int test_ieee(void);

int test_ieee(void){
  int i;
  char *old_env_gsl_ieee_mode;
  double x;
  int pass = 0;
 

  /* store GSL_IEEE_MODE */
#ifdef _GNU_SOURCE
  old_env_gsl_ieee_mode = secure_getenv("GSL_IEEE_MODE");
#else
  old_env_gsl_ieee_mode = getenv("GSL_IEEE_MODE");
#endif
  printf("Saving GSL_IEEE_MODE: %s\n", old_env_gsl_ieee_mode);
  
  setenv("GSL_IEEE_MODE",
         /*
            This is the recommended mode:
         */

         "double-precision,mask-denormalized,mask-underflow",

         /* These are for testing. */
         /* "double-precision,mask-underflow", */
         /* "double-precision,mask-denormalized", */
         /* "double-precision,mask-denormalized,mask-underflow,mask-division-by-zero", */
         1);
  gsl_ieee_env_setup();

  /* mask-underflow - allow
     trap-common - forbid
   */
  printf("exp(-3000.0) = %20.15g\n",
         exp(-3000.0));

  /* mask-denormalized - allow
     trap-common - forbid
   */
  x = 1e-306;
  for(i=1; i<70; i++){
    printf("i = %d  x = %20.15e\n",
           i,x);
    x /= 2;
  };

  /* mask-underflow - forbid
     trap-common - forbid
   */
  div_by_zero();
  /*
  printf("1.0/0.0 = %20.15g\n",
         1.0/0.0);
  */

  setenv("GSL_IEEE_MODE",
         old_env_gsl_ieee_mode,1); /* restore initial GSL_IEEE_MODE */
  printf("Restoring GSL_IEEE_MODE: %s\n", old_env_gsl_ieee_mode);
  printf("pass = %d\n",pass);
  
  return pass; /* floating point errors should cause failure before
                  reaching here */
}


int main(void){
  test_ieee();
  return 0;
}
