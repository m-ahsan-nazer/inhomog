/*
   test FLRW growth functions

   Copyright (C) 2013,2018 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_statistics.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif

#include <time.h>

#include "lib/inhomog.h"


/** test FLRW-background growth functions.

   The tests here are based on the reasoning that an EdS growth
   function q, with an algebraically trivial formula for q
   (identical to the scale factor a), should give nearly the
   same results as the growth function for a flat Lambda FLRW
   model in which Omega_Lambda is nearly, but not quite zero.
   Algorithmically, the EdS growth functions use the simple
   expressions, while the flat Lambda growth functions use
   numerical integration. In other words, the aim is to test
   the limiting case of numerical integration to see if it
   agrees - within an experimentally estimated tolerance -
   with the limit calculated in a much simpler way.
*/

int main(void){

  int want_verbose = 1;

#define N_TEST_T 30
#define N_MODELS 2

  /*
     For benchmarking, choose just one of these tests;
     otherwise timings for the different functions are
     averaged, which is not very useful.
   */
#define TEST_Q 1
#define TEST_Q_DOT 1
#define TEST_Q_DDOT 1
  /*
#define TEST_Q_DDOT 1
  */

  struct background_cosm_params_s background_cosm_params;
  int i_t;
  double delta_t;
  double t_i = 0.004574998; /* for z=200, H_0=50, EdS */
  double t_0 = 13.04;       /* for z=200, H_0=50, EdS */
  double t_background[N_TEST_T];

  /* test parameters */
  double q_test[N_MODELS][N_TEST_T];
  double q_dot_test[N_MODELS][N_TEST_T];
  double q_ddot_test[N_MODELS][N_TEST_T];

  double q_diff[N_TEST_T];
  double q_dot_diff[N_TEST_T];
  double q_ddot_diff[N_TEST_T];

  /* errors in (q_growth -q_growth_i) / q_growth_i; */
  double xi[2], xi_diff[N_TEST_T];
  /* errors in q_growth_dot / q_growth_i; */
  double xi_dot[2], xi_dot_diff[N_TEST_T];

  /* benchmarking */
  clock_t  benchmark[10]; /* num elements hardwired! */
  int  i_bench=0;


#define TEST_MAX_ERR 2e-4
#define TEST_MAX_ERR_DDOT 5e-4
#define TEST_MEDIAN_ERR 2e-4
  int pass = 0; /* default test status = OK = 0 */

  const double two_thirds = 2.0/3.0;
  const double five_sixths = 5.0/6.0;
  /* Bildhauer & Buchert 1992 Eq (31) normalisation:
     2 / sqrt(pi) * gamma(11/6) * gamma(2/3) */
  const double BildBuchEq31_norm = 1.43728308846099;

  int i_EdS;

  delta_t = (t_0-t_i)/((double)(N_TEST_T-1));

  background_cosm_params.flatFLRW = 1;

  background_cosm_params.inhomog_a_scale_factor_initial = 1.0/201.0;
  background_cosm_params.inhomog_a_d_scale_factor_initial = 1.0/201.0;
  background_cosm_params.inhomog_a_scale_factor_now = 1.0;

  benchmark[i_bench] = clock(); /* initialise timer */

  for(i_EdS=0; i_EdS<2; i_EdS++){
    background_cosm_params.EdS = i_EdS;

    i_bench ++;
    benchmark[i_bench]=clock();
    printf("beginning of i_EdS = %d loop\n",i_EdS);
    print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

    if(1 == background_cosm_params.EdS){
      background_cosm_params.H_0 = 50.0;
      background_cosm_params.Omm_0 = 1.0;
      background_cosm_params.OmLam_0 = 0.0;
    }else if(1 == background_cosm_params.flatFLRW){
      /* background_cosm_params.H_0 = 70.0;
         background_cosm_params.OmLam_0 = 0.73; */
      background_cosm_params.H_0 = 50.0;
      background_cosm_params.OmLam_0 = 0.001;
      background_cosm_params.Omm_0 = 1.0 - background_cosm_params.OmLam_0;
    };
    background_cosm_params.recalculate_t_0 = 1; /* initially recalculate for this test */

    for(i_t=0; i_t<N_TEST_T; i_t++){
      t_background[i_t] = t_i + (double)i_t * delta_t;

#ifdef TEST_Q
      /* test scale factor and growth function */
      q_test[i_EdS][i_t] = growth_FLRW(&background_cosm_params,
                                       t_background[i_t],
                                       want_verbose);
#endif
#ifdef TEST_Q_DOT
      q_dot_test[i_EdS][i_t] =
         dot_growth_FLRW(&background_cosm_params,
                         t_background[i_t],
                         want_verbose);
#endif
#ifdef TEST_Q_DDOT
      /* d^2 D/dt^2 */
      q_ddot_test[i_EdS][i_t] =
         ddot_growth_FLRW(&background_cosm_params,
                         t_background[i_t],
                         want_verbose);
#endif
    };  /*     for(i_t=0; i_t<N_TEST_T; i_t++) */

    i_bench ++;
    benchmark[i_bench]=clock();
    printf("...this test took: ");
    print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

  }; /*   for(i_EdS=0; i_EdS<2; i_EdS++) */

  i_bench ++;
  benchmark[i_bench]=clock();
  printf("...the last few lines took: ");
  print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

  printf("tcalc_LCDM/tcalc_EdS: %8u %7.2f \n",
         (uint)N_TEST_T,
         (double)(benchmark[2]-benchmark[1])/
         (double)(benchmark[4]-benchmark[3]));


  printf("\ntest_growth_function:\n");

  printf("Bildhauer & Buchert (31): coeff, full = %g %g\n",
         BildBuchEq31_norm *
         gsl_sf_beta_inc (five_sixths, two_thirds, 1.0),
         BildBuchEq31_norm *
         gsl_sf_beta_inc (five_sixths, two_thirds, 1.0) *
         exp( log(background_cosm_params.Omm_0 /
                  background_cosm_params.OmLam_0)/3.0 ) );


  printf("test growth functions: t q_LCDM q_EdS q_dot_LCDM q_dot_EdS\n");
  printf("background_cosm_params.OmLam_0 = %g\n",
         background_cosm_params.OmLam_0);
  for(i_t=0; i_t<N_TEST_T; i_t++){
    printf("q: %g  %g %g  %g %g  %g %g",
           t_background[i_t],
           q_test[0][i_t],
           q_test[1][i_t],
           q_dot_test[0][i_t],
           q_dot_test[1][i_t],
           q_ddot_test[0][i_t],
           q_ddot_test[1][i_t]);
    printf("\n");

    /* relative errors */
    q_diff[i_t] = fabs(q_test[0][i_t] - q_test[1][i_t]) * 0.5 /
      fabs(q_test[0][i_t] + q_test[1][i_t]);
    q_dot_diff[i_t] = fabs(q_dot_test[0][i_t] - q_dot_test[1][i_t]) * 0.5/
      fabs(q_dot_test[0][i_t] + q_dot_test[1][i_t]);
    q_ddot_diff[i_t] = fabs(q_ddot_test[0][i_t] - q_ddot_test[1][i_t]) * 0.5/
      fabs(q_ddot_test[0][i_t] + q_ddot_test[1][i_t]);

    if(i_t > 0){
      xi[0] = (q_test[0][i_t] - q_test[0][0])/q_test[0][0];
      xi[1] = (q_test[1][i_t] - q_test[1][0])/q_test[1][0];
      xi_diff[i_t] = fabs(xi[0]-xi[1])*0.5/(xi[0]+xi[1]);
    }else{
      xi[0] = 0.0;
      xi[1] = 0.0;
      xi_diff[i_t] = 0.0;
    };

    xi_dot[0] = q_dot_test[0][i_t]/q_test[0][0];
    xi_dot[1] = q_dot_test[1][i_t]/q_test[1][0];
    xi_dot_diff[i_t] = fabs(xi_dot[0]-xi_dot[1])*0.5/
      (xi_dot[0]+xi_dot[1]);

    /*    if(want_verbose){
      printf("xi, xi_dot rel err = %g %g\n",
             xi_diff[i_t],xi_dot_diff[i_t]);
             };
    */

  };

  gsl_sort(q_diff, 1, N_TEST_T);
  gsl_sort(q_dot_diff, 1, N_TEST_T);
  printf("median a adot, max a adot = %g %g %g %g\n",
           gsl_stats_median_from_sorted_data(q_diff, 1, N_TEST_T),
           gsl_stats_median_from_sorted_data(q_dot_diff, 1, N_TEST_T),
           gsl_stats_max(q_diff, 1, N_TEST_T),
           gsl_stats_max(q_dot_diff, 1, N_TEST_T));
  gsl_sort(q_ddot_diff, 1, N_TEST_T);
  printf("median addot max addot = %g %g\n",
      gsl_stats_median_from_sorted_data(q_ddot_diff, 1, N_TEST_T),
      gsl_stats_max(q_ddot_diff, 1, N_TEST_T));

  gsl_sort(xi_diff, 1, N_TEST_T);
  gsl_sort(xi_dot_diff, 1, N_TEST_T);
  printf("median xi xi_dot, max xi xi_dot = %g %g %g %g\n",
           gsl_stats_median_from_sorted_data(xi_diff, 1, N_TEST_T),
           gsl_stats_median_from_sorted_data(xi_dot_diff, 1, N_TEST_T),
           gsl_stats_max(xi_diff, 1, N_TEST_T),
           gsl_stats_max(xi_dot_diff, 1, N_TEST_T));


  /* automated test decision */
  if(gsl_stats_median_from_sorted_data(q_diff, 1, N_TEST_T) > TEST_MEDIAN_ERR) pass += 1;
  if(gsl_stats_median_from_sorted_data(q_dot_diff, 1, N_TEST_T) > TEST_MEDIAN_ERR) pass += 2;
  if(gsl_stats_median_from_sorted_data(q_ddot_diff, 1, N_TEST_T) > TEST_MEDIAN_ERR) pass += 4;
  if(gsl_stats_max(q_diff, 1, N_TEST_T) > TEST_MAX_ERR) pass += 8;
  if(gsl_stats_max(q_dot_diff, 1, N_TEST_T) > TEST_MAX_ERR) pass += 16;
  if(gsl_stats_max(q_ddot_diff, 1, N_TEST_T) > TEST_MAX_ERR_DDOT) pass += 32;

  if(gsl_stats_median_from_sorted_data(xi_diff, 1, N_TEST_T) > TEST_MEDIAN_ERR) pass += 64;
  if(gsl_stats_median_from_sorted_data(xi_dot_diff, 1, N_TEST_T) > TEST_MEDIAN_ERR) pass += 128;
  if(gsl_stats_max(xi_diff, 1, N_TEST_T) > TEST_MAX_ERR) pass += 256;
  if(gsl_stats_max(xi_dot_diff, 1, N_TEST_T) > TEST_MAX_ERR) pass += 512;

  if( !(isfinite(gsl_stats_median_from_sorted_data(q_diff, 1, N_TEST_T)) &&
        isfinite(gsl_stats_median_from_sorted_data(q_dot_diff, 1, N_TEST_T)) &&
        isfinite(gsl_stats_max(q_diff, 1, N_TEST_T)) &&
        isfinite(gsl_stats_max(q_dot_diff, 1, N_TEST_T)) &&
        isfinite(gsl_stats_median_from_sorted_data(xi_diff, 1, N_TEST_T)) &&
        isfinite(gsl_stats_median_from_sorted_data(xi_dot_diff, 1, N_TEST_T)) &&
        isfinite(gsl_stats_max(xi_diff, 1, N_TEST_T)) &&
        isfinite(gsl_stats_max(xi_dot_diff, 1, N_TEST_T))) ) pass += 1024;

  printf("test_growth_function: pass = %d\n",pass);
  return pass;
}
