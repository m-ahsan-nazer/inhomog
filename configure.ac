dnl Process this file with autoconf to produce a configure script.

AC_INIT([inhomog], [0.1.10])
dnl libtool library versioning:
INHOMOG_CURRENT=2
INHOMOG_REVISION=0
INHOMOG_AGE=2
AC_SUBST(INHOMOG_CURRENT)
AC_SUBST(INHOMOG_REVISION)
AC_SUBST(INHOMOG_AGE)
AC_CONFIG_SRCDIR([inhomog.c])
dnl Every other copy of the package version number gets its value from here
AM_INIT_AUTOMAKE

AC_CONFIG_MACRO_DIRS([m4])

dnl libtool:
LT_INIT
INHOMOG_LT_VERSION="${INHOMOG_CURRENT}:${INHOMOG_REVISION}:${INHOMOG_AGE}"
AC_SUBST(INHOMOG_LT_VERSION)
dnl
dnl inhomog-0.1.2  libinhomog-0.0.0

dnl See https://www.gnu.org/software/libtool/manual/html_node/Updating-version-info.html
dnl as summarised in the GSL configure.ac file:
dnl
dnl How to update library version number
dnl ====================================
dnl
dnl C: increment if the interface has additions, changes, removals.
dnl
dnl R: increment any time the source changes; set to 0 if you
dnl incremented CURRENT
dnl
dnl A: increment if any interfaces have been added; set to 0 if any
dnl interfaces have been removed. removal has precedence over adding,
dnl so set to 0 if both happened.
dnl
dnl 2018-12-22: it seems that -version-info C:R:A results in a triple
dnl of version-related numbers PACKAGE.so.(C-A).A.R
dnl



AC_ARG_ENABLE([devmode],
  [  --enable-devmode   Compiler options for development only],
  [case "${enableval}" in
    yes) devmode=true ;;
    no)  devmode=false ;;
    *) AC_MSG_ERROR([bad value ${enableval} for --enable-devmode]) ;;
  esac],[devmode=false])
# AM_CONDITIONAL([DEV_MODE], [test 1 = 1 ]) # to hardwire
AM_CONDITIONAL([DEV_MODE], [test x${devmode} = xtrue ])

dnl create a config.h file (Automake will add -DHAVE_CONFIG_H)
AM_CONFIG_HEADER(config.h)

#Recommendation against AM_MAINTAINER_MODE:
# https://www.gnu.org/software/automake/manual/html_node/maintainer_002dmode.html
#AM_MAINTAINER_MODE

AC_SUBST(VERSION)

ISODATE=`date +%Y-%m-%d`
AC_SUBST(ISODATE)

AC_CANONICAL_HOST

dnl Checks for programs.
AC_PROG_INSTALL
AC_PROG_CC

dnl Checks for libraries.

AC_CHECK_LIB(gcc,main,,
        [AC_MSG_ERROR(Could not find libgcc.* - needed especially on Solaris systems with f77 - hint: something like adding  -L/opt/lib/gcc-lib/sparc-sun-solaris2.6/2.7.2.3/ to LDFLAGS )
        ])

AC_CHECK_LIB([m],[cos],,)

AC_CHECK_LIB(gslcblas,cblas_dgemv)
AC_CHECK_LIB(gsl,gsl_spline_init,,
        [AC_MSG_ERROR(Could not find gsl. Hint (1)  find /usr -name "*gsl*" -print   Hint (2)  install gsl*.a  e.g. from  http://www.gnu.org/software/gsl/ and put it in /usr/local/lib/ or set environment variable LDFLAGS to be -L/my/directory/lib/ with the library and CFLAGS to be -I/my/directory/include for the include files.)
        ])

AC_CHECK_LIB(gsl,gsl_sort2,
        [AC_DEFINE([HAVE_GSL_SORT2],[1],
           [GSL library such as 2.3 or later that has gsl_sort2.])],
        [AC_MSG_WARN([You have an old version of GSL, which does not include gsl_sort2. Please consider updating GSL soon.])])

#AC_CHECK_LIB(fftw3,fftw_plan_dft_3d,,
#        [AC_MSG_ERROR(Could not find fftw3. Hint (1)  find /usr -name "*fftw3*" -print   Hint (2)  install fftw3*.a  e.g. from  http://www.fftw.org and put it in /usr/local/lib/ or set environment variable LDFLAGS to be -L/my/directory/lib/ with the library and CFLAGS to be -I/my/directory/include for the include files.)
#        ])



dnl Remove explicit dependence on external libraries unless they are big or likely to be updated/debugged significantly.
dnl AC_CHECK_LIB([cosmdist],[cosm_get_comov_dist],,
dnl     [
dnl     AC_MSG_WARN(Could not find cosmdist - Try checking at  http://cosmo.torun.pl/GPLdownload/dodec/ and put it in /usr/local/lib/ or set environment variable LDFLAGS to be -L/my/directory/lib/ with the library and CPPFLAGS to be -I/my/directory/include for the include files.)])
dnl
dnl
dnl AC_CHECK_LIB([astromisc],[parse_noempty_strtox],,
dnl     [
dnl     AC_MSG_WARN(Could not find astromisc - Try checking at  http://cosmo.torun.pl/GPLdownload/dodec/ and put it in /usr/local/lib/ or set environment variable LDFLAGS to be -L/my/directory/lib/ with the library and CPPFLAGS to be -I/my/directory/include for the include files.)])
dnl

AC_OPENMP


dnl Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS(unistd.h sys/param.h sys/time.h time.h sys/mkdev.h sys/sysmacros.h string.h memory.h fcntl.h dirent.h sys/ndir.h ndir.h alloca.h locale.h )

jm_CHECK_TYPE_STRUCT_UTIMBUF
AC_HEADER_MAJOR
AC_FUNC_ALLOCA
AC_STRUCT_TM
AC_STRUCT_ST_BLOCKS
AC_FUNC_CLOSEDIR_VOID
AC_CHECK_FUNCS(mkfifo)
AC_CHECK_FUNC(mknod)
AC_CHECK_HEADER(argp.h,,AC_MSG_ERROR(Cannot continue: argp.h not found - please install glibc - http://directory.fsf.org/libs/c/glibc.html))

dnl Checks for typedefs, structures, and compiler characteristics.

AC_PROG_MAKE_SET

# AC_PROG_RANLIB - obsolete according to autoconf 2.69

dnl Checks for library functions.

AC_CONFIG_FILES([Makefile lib/Makefile scripts/Makefile])
AC_OUTPUT
