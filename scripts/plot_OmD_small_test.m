global printable=1
global powerdot=0 ## for powerdot slide presentations
global lin_theory_threshold = 1.69
global enable_pause=0

input_files = {"OmD_examp_20151011_0.00746269_all_tmp.dat",
	       "OmD_examp_20151011_0.0160779_all_tmp.dat",
	       "OmD_examp_20151011_0.0346387_all_tmp.dat"};

graphics_toolkit('gnuplot');

colour_order = [0.00000   0.00000   1.00000
		0.00000   0.50000   0.00000
		1.00000   0.00000   0.00000
		0.00000   0.70000   0.75000
		0.75000   0.00000   0.75000
		0.75000   0.75000   0.00000
		0.25000   0.25000   0.25000
		0         0         0.00000];


plot([0 1],[0 1])
if(printable)
  set(gcf(),"defaultlinelinewidth",5);
elseif(powerdot)
  set(gcf(),"defaultlinelinewidth",18);
endif
set(gcf(),"defaultlinemarkersize",12);
set(gcf(),"defaulttextfontsize",28);
set(gcf(),"defaultaxesfontsize",32);
set(gcf(),"defaultaxeslinewidth",3);
set(gcf(),"defaulttextinterpreter",'tex');
set(gcf(),"defaultaxesinterpreter",'latex');

default_colour_order= get(gca()).colororder;

## linear theory 1.69 correction: fraction of positive fluctuations
## above the linear theory threshold:
fraction_above_positive_threshold = erfc(lin_theory_threshold/sqrt(2.0));


for i_file = 1:length(input_files)
  clear OmD;
  OmD = load(input_files{i_file});
  axes = [0 10 0 1];

  ## BKS case
  for i_KBS=0:6
    switch (i_KBS)
      case 6
	fract = fraction_above_positive_threshold;
      otherwise
	fract = 1.0;
    endswitch
    index = find((OmD(:,1)==1) .* (OmD(:,3)==i_KBS));
    set(gca(),"colororder",colour_order(i_KBS+1,:));
    plot(OmD(index,4), OmD(index,5).*fract, ["-;" sprintf("%d",i_KBS) ";"])
    hold on
  endfor
  axis(axes)
  xlabel('z')
  ylabel('F')
  title('initBKS')
  set(gca(),'xdir','reverse')
  print(['/tmp/OmD_BKS_' sprintf("%1d",i_file) '.eps'],
	 "-depsc","-FHelvetica:32")
  hold off
  if(enable_pause)pause endif

  ## Raychaudhuri evolution case
  for i_curv=0:1
    for i_KBS=0:6
      switch (i_KBS)
	case 6
	  fract = fraction_above_positive_threshold;
	otherwise
	  fract = 1.0;
      endswitch
      index = find((OmD(:,1)==0) .* (OmD(:,2)==i_curv) .* (OmD(:,3)==i_KBS));
      set(gca(),"colororder",colour_order(i_KBS+1,:));
      plot(OmD(index,4), OmD(index,5).*fract, ["-;" sprintf("i_: %d",i_KBS) ";"])
      hold on
    endfor
    axis(axes)
    xlabel('z')
    ylabel('F')
    title(['initHam ' sprintf("curv%d",i_curv)])
    set(gca(),'xdir','reverse')
    print(['/tmp/OmD_Ray_' sprintf("curv%1d_KBS%1d",i_curv,i_file) '.eps'],
	 "-depsc","-FHelvetica:32")
    hold off
    if(enable_pause)pause  endif
  endfor # for i_curv=0:1
endfor # for i_file = 1:length(input_files)

  

